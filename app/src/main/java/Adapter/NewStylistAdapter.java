package Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.dis015.mdxprestigesalon5.HomeActivity;
import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import FirstFlowFragments.MyapptsFragment;
import ModelClass.BookingsCollection;
import ModelClass.StylistCollection;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

public class NewStylistAdapter extends RecyclerView.Adapter{
   private ArrayList<String> endTimeList;
    private String date;
    private String startTime;
    private ArrayList<String> servicesId;
    private Integer price;
    private ArrayList<Integer> durationArrayList;
    private ArrayList<String> stylistArrayList;
    private CircleImageView stylistImg;
    private Integer intDuration;
    private List<StylistCollection> stylistDetailsList;
    private Context context;
    Button bookingPopup;
    int position;



    public NewStylistAdapter(Context context, String date, String startTime, ArrayList<String> servicesId,
                             Integer price, ArrayList<String> stylistArrayList,
                             ArrayList<Integer> durationArrayList,
                             ArrayList<String> endTimeList,
                             List<StylistCollection> stylistDetailsList,
                             CircleImageView stylistImg) {
        this.context=context;
        this.date=date;
        this.startTime=startTime;
        this.servicesId=servicesId;
        this.price=price;
        this.stylistArrayList=stylistArrayList;
        this.durationArrayList=durationArrayList;
        this.endTimeList=endTimeList;
        this.stylistDetailsList=stylistDetailsList;
        this.stylistImg=stylistImg;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem, parent, false);
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((listViewHolder)holder).stylistname.setText(stylistDetailsList.get(position).getFirstName());
        ((listViewHolder)holder).lastnameTv.setText(stylistDetailsList.get(position).getLastName());
        Picasso.get().load(stylistDetailsList.get(position).getImageUrl()).into(((listViewHolder) holder).stylistImg);

    }

    @Override
    public int getItemCount() {
        return stylistDetailsList.size() ;
    }

    public class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView stylistname, lastnameTv;
        private ImageView stylistImg;
        private Button ChooseBtn;
        private ProgressBar progressBar;

        public listViewHolder(View itemView) {
            super(itemView);
            stylistImg = itemView.findViewById(R.id.stylistImg);
            stylistname = itemView.findViewById(R.id.StylistTv);
            lastnameTv = itemView.findViewById(R.id.lastnameTv);
            progressBar = itemView.findViewById(R.id.Progressbar);
            ChooseBtn = itemView.findViewById(R.id.chooseBtn);
            ChooseBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            position=getAdapterPosition();

            showPopup();

        }

    }

    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;

    }

    private void showPopup() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.confirm_booking);
        dialog.show();
        bookingPopup=dialog.findViewById(R.id.okBtn);
        bookingPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strImageUrl=stylistDetailsList.get(position).getImageUrl();
                Picasso.get().load(strImageUrl).into(stylistImg);
                String strStylistId=stylistDetailsList.get(position).getStylistid();

                String endTime=endTimeList.get(position);
                Integer duration=durationArrayList.get(position);
                String strDate=formateDateFromstring("EE dd, MMM yyyy","yyyy-MM-dd",date);


                HashMap<String, String> recurring;
                recurring = new HashMap<>();
                recurring.put("startDate",strDate);
                recurring.put("endDate","dssasa");
                recurring.put("type","fsdsafds");

                //to get current time and date
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                Date currentLocalTime = cal.getTime();
                String strTime = currentLocalTime.toString();
                String strCurrenttime;
                strCurrenttime = formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy", "yyyMMddZHHmmss", strTime);
                String recurringId =strCurrenttime.concat(strStylistId);

                FirebaseAuth firebaseAuth= FirebaseAuth.getInstance();
                String strCustomer=firebaseAuth.getUid();

                BookingsCollection bookingsCollection = new BookingsCollection("mobile",
                        "booking","",strCustomer,strDate,duration,
                        endTime, strImageUrl,price, recurring,recurringId, servicesId,startTime,strStylistId);
                Log.i("book", String.valueOf(bookingsCollection));

                FirebaseFirestore db= FirebaseFirestore.getInstance();
                db.collection("bookings").add(bookingsCollection).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        MyapptsFragment bookingsFragment = new MyapptsFragment();
                        AppCompatActivity activity = (AppCompatActivity)context;
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, bookingsFragment).addToBackStack(null).commit();
                        ((HomeActivity)context).changeMenu(R.id.navigation_apps);
                        dialog.dismiss();

                    }
                });
            }
        });
    }
}
