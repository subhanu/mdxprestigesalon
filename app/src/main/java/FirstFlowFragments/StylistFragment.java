package FirstFlowFragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import Adapter.StylistAdapter;
import ModelClass.DividerItemDecorator;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class StylistFragment extends Fragment {

    private RecyclerView recyclerView;
    private String documentId;
    CircleImageView stylistImg;
    SharedPreferences sharedPreferences;
    ImageView backImg;


    public StylistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.stylist_recycler, container, false);
        recyclerView=view.findViewById(R.id.stylistRecycler);

        backImg=view.findViewById(R.id.back_Img);
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getContext());
        documentId=sharedPreferences.getString("appointmentid","");

        try{
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            try {
                db.collection("stylists").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            try {
                                ArrayList<String> stylistId=new ArrayList<>();
                                for(DocumentSnapshot documentSnapshot:task.getResult()){
                                    String id=documentSnapshot.getId();
                                    stylistId.add(id);
                                }
                                StylistAdapter listAdapter = new StylistAdapter(task,documentId,stylistId,stylistImg);
                                RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getContext(), R.drawable.divider));
                                recyclerView.addItemDecoration(dividerItemDecoration);
                                recyclerView.setAdapter(listAdapter);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                                listAdapter.notifyDataSetChanged();

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            sharedPreferences.edit().clear().apply();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeFragment = new HomeFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, homeFragment).addToBackStack(null).commit();

            }
        });

        return view;
    }
}
