package com.example.dis015.mdxprestigesalon5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class SplashScreenActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(5*1000);
                    // After 5 seconds redirect to another intent
                    Intent i=new Intent(SplashScreenActivity.this, PhoneNumberLogin.class);
                    startActivity(i);

                    //Remove activity
                    finish();

                }
                catch (Exception e) {

                }
            }
        };

        // start thread
        background.start();
    }


}

