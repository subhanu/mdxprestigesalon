package SecondFlowFragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import Adapter.DifferentGenderServicesAdapter2;
import ModelClass.BookingDetails;
import ModelClass.HttpHandler;
import ModelClass.StylistArray;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment3 extends Fragment {

    CircleImageView imageView;
    TextView stylistNameTv,servicesTv,durationTv,dateTv,timeTv,error_tv;
    Button serviceButton;
    ImageView backImg;

    RecyclerView recyclerView;
    private DifferentGenderServicesAdapter2 servicesAdapter2;
    LinearLayout menLayout,womenLayout,childrenLayout;
    ArrayList<String> servicesId=new ArrayList<>();
    ProgressBar progressBar;
    int position;
    String strStartTime,strDate,startDate;
    Bundle bundle;
    ArrayList<String> stylistId=new ArrayList<>();
    ArrayList<String> responseArr=new ArrayList<>();
    ArrayList<ArrayList<String>> selectedStylistId=new ArrayList<>();
    ArrayList<ArrayList<Integer>> selectedDuration=new ArrayList<>();
    ArrayList<String> endTimeArr=new ArrayList<>();
    ArrayList<Integer> duration=new ArrayList<>();


    public ServicesFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.servicesfrag, container, false);
        try {
            stylistNameTv =view.findViewById(R.id.stylistNameTv);
            servicesTv =  view.findViewById(R.id.serviceTv);
            durationTv = view.findViewById(R.id.durationTv);
            imageView =  view.findViewById(R.id.stylistImg);
            menLayout=view.findViewById(R.id.menLayout);
            womenLayout=view.findViewById(R.id.womenLayout);
            childrenLayout=view.findViewById(R.id.childrenLayout);
            serviceButton = view.findViewById(R.id.servicesBtn);
            recyclerView =view.findViewById(R.id.stylistRecycler);
            backImg=view.findViewById(R.id.back_Img);

            progressBar=view.findViewById(R.id.progressBar);
            dateTv=view.findViewById(R.id.dateTv);
            timeTv=view.findViewById(R.id.timeTv);
            error_tv=view.findViewById(R.id.error_tv);


            bundle=getArguments();
            if(bundle!=null){
                strDate=bundle.getString("date","");
                strStartTime=bundle.getString("time","");
            }

            dateTv.setText(strDate);
            timeTv.setText(strStartTime);

            String dateFormat=formateDateFromstring("EE dd, MMM yyyy","yyyy-MM-dd",strDate);
            startDate=dateFormat;
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if(task.isSuccessful()){

                        for(DocumentSnapshot documentSnapshot:task.getResult()){
                            String document=documentSnapshot.getId();
                            servicesId.add(document);
                        }

                        servicesAdapter2=new DifferentGenderServicesAdapter2(task,servicesId, servicesTv, durationTv,error_tv);
                        recyclerView.setAdapter(servicesAdapter2);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        try{
            menLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    menLayout.setBackgroundResource(R.drawable.white_background);
                    womenLayout.setBackgroundResource(R.color.darkgrey);
                    childrenLayout.setBackgroundResource(R.color.darkgrey);
                    position=0;

                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    db.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                ArrayList<String> strservicesArray=new ArrayList<>();

                                for(DocumentSnapshot documentSnapshot:task.getResult()){
                                    String document=documentSnapshot.getId();
                                    strservicesArray.add(document);
                                }
                                servicesAdapter2=new DifferentGenderServicesAdapter2(task,strservicesArray, servicesTv, durationTv, error_tv);
                                recyclerView.setAdapter(servicesAdapter2);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            });
            womenLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);

                    position=1;
                    womenLayout.setBackgroundResource(R.drawable.white_background);
                    menLayout.setBackgroundResource(R.color.darkgrey);
                    childrenLayout.setBackgroundResource(R.color.darkgrey);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("services_women").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                ArrayList<String> strservicesArray=new ArrayList<>();

                                for(DocumentSnapshot documentSnapshot:task.getResult()){
                                    String document=documentSnapshot.getId();
                                    strservicesArray.add(document);
                                }
                                servicesAdapter2=new DifferentGenderServicesAdapter2(task,strservicesArray, servicesTv, durationTv, error_tv);
                                recyclerView.setAdapter(servicesAdapter2);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                                progressBar.setVisibility(View.GONE);

                            } }}); }
            });
            childrenLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    position=2;
                    progressBar.setVisibility(View.VISIBLE);

                    childrenLayout.setBackgroundResource(R.drawable.white_background);
                    womenLayout.setBackgroundResource(R.color.darkgrey);
                    menLayout.setBackgroundResource(R.color.darkgrey);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("services_children").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                ArrayList<String> strservicesArray=new ArrayList<>();

                                for(DocumentSnapshot documentSnapshot:task.getResult()){
                                    String document=documentSnapshot.getId();
                                    strservicesArray.add(document);
                                }


                                servicesAdapter2=new DifferentGenderServicesAdapter2(task,strservicesArray, servicesTv, durationTv, error_tv);
                                recyclerView.setAdapter(servicesAdapter2);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(layoutManager);
                                progressBar.setVisibility(View.GONE);

                            }
                        }});
                }});
        }catch(Exception e){
            e.printStackTrace();
        }

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDateFragment2 timeDateFragment2 = new TimeDateFragment2();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, timeDateFragment2).addToBackStack(null).commit();

            }
        });


        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedStylistId=servicesAdapter2.getSelectedStylistId();
                selectedDuration=servicesAdapter2.getSelectedDuration();
                commonId(selectedStylistId);
                new freeBusy().execute();
                try {
                    if (servicesAdapter2.getServices() == null) {
                        Toast.makeText(getContext(), "Please Choose your service", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        return view;
    }


    public void commonId(ArrayList<ArrayList<String>> lists) {
//        List<String> common = new ArrayList<>(lists.get(0));
        ArrayList<String> common=new ArrayList<>(lists.get(0));
        if (lists.size() > 1){
            common.retainAll(lists.get(1));
            stylistId=common;
        }else {
            for(ArrayList<String> l : lists) {
                for(String s : l) {
                    stylistId.add(s);
                }
            }
        }

    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed;
        String outputDate ="";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;

    }



    private class freeBusy extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            ArrayList<String> stylistArrList=new ArrayList<>();
            ArrayList<Integer> durationArrList=new ArrayList<>();

            for(ArrayList<String> l:selectedStylistId) {
                for(String s : l) {
                    stylistArrList.add(s);
                }
            }

            for (ArrayList<Integer> l:selectedDuration){
                for(Integer s:l){
                    durationArrList.add(s);
                }
            }

            for(int i=0;i<stylistId.size();i++){
                for(int j=0;j<stylistArrList.size();j++){
                    if(stylistId.get(i).equals(stylistArrList.get(j))){
                        Integer intDuration=durationArrList.get(j);
                        duration.add(intDuration);
                    }
                }
            }

            //calculate end time for each stylist
            for(int i=0;i<duration.size();i++){
                String strServiceDuration=formateDateFromstring("mm", "HH:mm", duration.get(i).toString());

                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                String strToatalDuration;
                try {
                    //add service duration with selected time to calculate endtime
                    Date date1 = dateFormat.parse(strStartTime);
                    Date date2 = dateFormat.parse(strServiceDuration);
                    long sum = date1.getTime() + date2.getTime();
                    strToatalDuration = dateFormat.format(new Date(sum));
                    endTimeArr.add(strToatalDuration);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }


            ArrayList<StylistArray> stylistsArr=new ArrayList<StylistArray>();

            for(int i=0;i<stylistId.size();i++){
                stylistsArr.add(new StylistArray(stylistId.get(i),endTimeArr.get(i)));
            }


            List<StylistArray> list = new ArrayList<>();
            JSONArray jsonArray = new JSONArray();
            try {
                // You need to use simple for loop instead the following foreach
                // because foreach is slower than traditional loop.
                for(int i=0;i<stylistsArr.size();i++){
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("stylist",stylistsArr.get(i).getStylist());
                    jsonObject.put("endTime", stylistsArr.get(i).getEndTime());
                    jsonArray.put(jsonObject);
                    Log.e("JSON", String.valueOf(jsonArray));


                }
            } catch (JSONException e) {
                e.printStackTrace();
                // Error happens, try to handle it.
            }

            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url= "https://us-central1-mdxprestigesalon5.cloudfunctions.net/freeBusyFlow2?date="+startDate+"&startTime="+strStartTime+"&stylistsArr="+jsonArray.toString();
            Log.e("url",url);

            String jsonStr = sh.makeServiceCall(url);
            Log.e(TAG, "Response from url: " + jsonStr);

            if(jsonStr!=null){
                try {
                    JSONArray jsonArray1=new JSONArray(jsonStr);
                    for(int i=0;i<jsonArray1.length();i++){
                        JSONObject jsonObj = jsonArray1.getJSONObject(i);
                        String status=jsonObj.getString("status");
                        responseArr.add(status);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(responseArr.size()!=0){

                for(int i= responseArr.size() - 1;i >= 0; i--){
                    if(responseArr.get(i).equals("busy")){
                        stylistId.remove(i);
                        endTimeArr.remove(i);
                        duration.remove(i);
                    }
                }


            }
            BookingDetails appointmentBooking=new BookingDetails(strDate,strStartTime,servicesAdapter2.getServices(),servicesAdapter2.getServiceId(),stylistId,endTimeArr,duration,servicesAdapter2.getCostArray());
            StylistFragment3 stylistFragment3=new StylistFragment3();
            bundle.putParcelable("bookingDetails",appointmentBooking);
            stylistFragment3.setArguments(bundle);
            getFragmentManager().beginTransaction().replace(R.id.frameLayout,stylistFragment3).addToBackStack(null).commit();

        }
    }

}



