package com.example.dis015.mdxprestigesalon5;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.concurrent.TimeUnit;

public class VerifyOtpActivity extends AppCompatActivity{
    EditText otp_Edt;
    Button verifyOtp_Btn;
    String verificationCode,strPhoneNumber,mVerificationId;
    FirebaseAuth firebaseAuth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final String TAG = "FirebasePhoneNumAuth";
    PhoneAuthProvider.ForceResendingToken mResendToken;
    TextView resendOtp;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        otp_Edt=findViewById(R.id.phoneNoEdt);
        verifyOtp_Btn=findViewById(R.id.phoneNoLoginBtn);
        resendOtp=findViewById(R.id.resendotp);
        progressBar=findViewById(R.id.progressBar);

        otp_Edt.setSingleLine();
        firebaseAuth= FirebaseAuth.getInstance();

        verificationCode=getIntent().getExtras().getString("verificationcode");
        strPhoneNumber=getIntent().getExtras().getString("phonenumber");

        verifyOtp_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                String input_code=otp_Edt.getText().toString();
                if(verificationCode!=null){
                    verifyPhoneNumber(verificationCode,input_code);
                }

            }
        });

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendVerificationCode(strPhoneNumber,mResendToken);
            }
        });
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                // ...
            }
        };


    }
    public void verifyPhoneNumber(String verificationCode,String input_code){
        PhoneAuthCredential credential= PhoneAuthProvider.getCredential(verificationCode,input_code);
        signinwithcredential(credential);
    }

    public void signinwithcredential(PhoneAuthCredential credential){
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    checkUserExixt();
                }

            }
        });
    }

    private void checkUserExixt() {
        final String uid = firebaseAuth.getUid();
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();

        firebaseFirestore.collection("customers").whereEqualTo("phone",strPhoneNumber).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    int count = 0;
                    for(QueryDocumentSnapshot document : task.getResult()){
                        Log.d("data" , document.getId() + " =>" + document.getData());
                        count++;
                    }
                    if(count>=1){
                        Log.d("succesfuluid",uid);
                        progressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }else if(count==0){
                        Log.d("unsuccesfuluid",uid);
                        progressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(getApplicationContext(), CustomerRegistration.class);
                        intent.putExtra("mobilenumber",strPhoneNumber);
                        startActivity(intent);
                        finish();
                    }

                }
            }
        });
    }
    private void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        strPhoneNumber=phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
}
