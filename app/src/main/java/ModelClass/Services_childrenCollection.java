package ModelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Services_childrenCollection implements Parcelable {

    private String serviceId;
    private String name;
    private Integer price;
    private ArrayList<String> duration;
    private ArrayList<String> stylist;



    public Services_childrenCollection(String serviceId, String name, Integer price, ArrayList<String> duration, ArrayList<String> stylist) {
        this.serviceId = serviceId;
        this.name = name;
        this.price = price;
        this.duration = duration;
        this.stylist = stylist;
    }

    public Services_childrenCollection() {
    }

    protected Services_childrenCollection(Parcel in) {
        serviceId = in.readString();
        name = in.readString();
        price = in.readInt();
        duration = in.createStringArrayList();
        stylist = in.createStringArrayList();
    }

    public static final Creator<Services_childrenCollection> CREATOR = new Creator<Services_childrenCollection>() {
        @Override
        public Services_childrenCollection createFromParcel(Parcel in) {
            return new Services_childrenCollection(in);
        }

        @Override
        public Services_childrenCollection[] newArray(int size) {
            return new Services_childrenCollection[size];
        }
    };

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ArrayList<String> getDuration() {
        return duration;
    }

    public void setDuration(ArrayList<String> duration) {
        this.duration = duration;
    }

    public ArrayList<String> getStylist() {
        return stylist;
    }

    public void setStylist(ArrayList<String> stylist) {
        this.stylist = stylist;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceId);
        dest.writeString(name);
        dest.writeInt(price);
        dest.writeStringList(duration);
        dest.writeStringList(stylist);
    }
}
