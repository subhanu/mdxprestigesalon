package SecondFlowFragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import Adapter.NewStylistAdapter;
import FirstFlowFragments.StylistFragment;
import ModelClass.BookingDetails;
import ModelClass.DividerItemDecorator;
import ModelClass.StylistCollection;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StylistFragment3 extends Fragment {


    ArrayList<String> services=new ArrayList<>();
    ArrayList<String> servicesId=new ArrayList<>();
    ArrayList<String> stylistidArray=new ArrayList<>();
    ArrayList<Integer> durationList=new ArrayList<>();
    Integer price;
    CircleImageView stylistImg;
    TextView serviceTv,dateTv,stylistNameTv,timeTv;
    ImageView backImg;
    Bundle bundle;
    BookingDetails bookingDetails;
    String date,startTime;
    ArrayList<String> endTimeList=new ArrayList<>();
    List<StylistCollection> stylistDetailsList;
    RecyclerView recyclerView;
    NewStylistAdapter newStylistAdapter;


    public StylistFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.stylist_fragment, container, false);

        bundle=getArguments();
        if(bundle!=null){
            bookingDetails=(BookingDetails)bundle.getParcelable("bookingDetails");
        }

        recyclerView =  view.findViewById(R.id.stylistRecycler);
        stylistImg=view.findViewById(R.id.stylistImg);
        serviceTv=view.findViewById(R.id.serviceTv);
        stylistNameTv=view.findViewById(R.id.stylistNameTv);
        dateTv=view.findViewById(R.id.dateTv);
        timeTv=view.findViewById(R.id.timeTv);
        backImg=view.findViewById(R.id.back_Img);



        try{
            date=bookingDetails.getDate();
            startTime=bookingDetails.getStartTime();
            services=bookingDetails.getServices();
            servicesId=bookingDetails.getServicesId();
            stylistidArray=bookingDetails.getStylistId();
            durationList=bookingDetails.getDuration();
            price=bookingDetails.getPrice();
            endTimeList=bookingDetails.getEndTime();
        }catch (Exception e){
            e.printStackTrace();
        }


        dateTv.setText(date);
        timeTv.setText(startTime);
        if(services.size()==2){
            serviceTv.setText(services.get(0)+" "+"+"+"\n"+services.get(1));

        }else {
            serviceTv.setText(services.toString().replaceAll("\\[|\\]", ""));

        }
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicesFragment3 servicesFragment3 = new ServicesFragment3();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, servicesFragment3).addToBackStack(null).commit();

            }
        });

        loadData();


        return view;
    }


    private void loadData(){
        setupAdapter();
        if(stylistidArray.size()==0){
            AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
            builder.setTitle("OOPS!");
            builder.setMessage("There is no stylist at this time.Please select stylist");
            builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    StylistFragment bookingsFragment = new StylistFragment();
                    AppCompatActivity activity = (AppCompatActivity)getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, bookingsFragment).addToBackStack(null).commit();

                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else {
            for(int i=0;i<stylistidArray.size();i++){
                FirebaseFirestore db= FirebaseFirestore.getInstance();
                db.collection("stylists").document(stylistidArray.get(i)).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            DocumentSnapshot documentSnapshot=task.getResult();
                            if(documentSnapshot.exists()){
                                String stylistid=documentSnapshot.getId();
                                String firstName=documentSnapshot.getString("firstName");
                                String lastName=documentSnapshot.getString("lastName");
                                String imageUrl=documentSnapshot.getString("imageUrl");
                                StylistCollection stylistCollection=new StylistCollection(stylistid,firstName,lastName,imageUrl);
                                stylistDetailsList.add(stylistCollection);
                                newStylistAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
            }

        }
    }

    private void setupAdapter(){
        stylistDetailsList = new ArrayList<>();
        newStylistAdapter = new NewStylistAdapter(getContext(),date,startTime,servicesId,price,stylistidArray,durationList,endTimeList,stylistDetailsList,stylistImg);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(newStylistAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }



}


