package com.example.dis015.mdxprestigesalon5;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import FirstFlowFragments.AccountFragment;
import FirstFlowFragments.HairtipsFragment;
import FirstFlowFragments.HomeFragment;
import FirstFlowFragments.MyapptsFragment;
import FirstFlowFragments.StylistFragment;
import ModelClass.BottomNavigationViewHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    BottomNavigationView bottomNavigationView;
    FirebaseAuth firebaseAuth;
    NavigationView navigationView;
    String strImageUrl,uid;
    TextView userName_tv;

    ArrayList<String> tokenArray=new ArrayList<>();
    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    boolean isUserFirstTime;
    CircleImageView img_profile;
    Toolbar toolbar;
    DrawerLayout drawerLayout;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    HomeFragment homeFragment=new HomeFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout,homeFragment).commit();
                    return true;

                case R.id.navigation_stylist:
                    StylistFragment stylistsFragment=new StylistFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction1=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction1.replace(R.id.frameLayout,stylistsFragment).commit();
                    return true;

                case R.id.navigation_apps:
                    MyapptsFragment myaaptsFragment=new MyapptsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction2=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.frameLayout,myaaptsFragment).commit();
                    return true;

                case R.id.navigation_tips:
                    HairtipsFragment hairtipsFragment=new HairtipsFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction3=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction3.replace(R.id.frameLayout,hairtipsFragment).commit();
                    return true;

                case R.id.navigation_account:
                    AccountFragment accountFragment=new AccountFragment();
                    android.support.v4.app.FragmentTransaction fragmentTransaction4=getSupportFragmentManager().beginTransaction();
                    fragmentTransaction4.replace(R.id.frameLayout,accountFragment).commit();

                    return true;
            }
            return false;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//         checkUserExist();

        //start onboarding when app is opening first time
        isUserFirstTime = Boolean.valueOf(Utils.readSharedSetting(HomeActivity.this, PREF_USER_FIRST_TIME, "true"));
        Intent introIntent = new Intent(HomeActivity.this, OnboardingActivity.class);
        introIntent.putExtra(PREF_USER_FIRST_TIME, isUserFirstTime);

        if (isUserFirstTime)
            startActivity(introIntent);

        setContentView(R.layout.home_layout);

       drawerLayout=findViewById(R.id.drawer_layout);

        bottomNavigationView = findViewById(R.id.navigation);
        navigationView=findViewById(R.id.nav_view);

        firebaseAuth=FirebaseAuth.getInstance();
          uid=firebaseAuth.getUid();

        final View hView =  navigationView.inflateHeaderView(R.layout.nav_header_main);
        img_profile=hView.findViewById(R.id.img_profile);
        userName_tv =hView.findViewById(R.id.name);

        //bottom navigationview listener
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        //navigation drawer listener
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(HomeActivity.this, new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        final String newToken = instanceIdResult.getToken();
                        Log.i("newToken",newToken);

                        FirebaseFirestore fb=FirebaseFirestore.getInstance();
                        fb.collection("customers").whereEqualTo("uid",uid)
                                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    for(DocumentSnapshot documentSnapshot:task.getResult()){
                                        String id=documentSnapshot.getId();
                                        String firstName=documentSnapshot.getString("firstName");
                                        String lastName=documentSnapshot.getString("lastName");
                                        String strCustomername=firstName.concat(" ").concat(lastName);
                                        strImageUrl=documentSnapshot.getString("imageUrl");
                                        Picasso.get().load(strImageUrl).into(img_profile);
                                        userName_tv.setText(strCustomername);
                                        tokenArray=(ArrayList<String>)documentSnapshot.get("fcmToken");
                                        if(tokenArray.contains(newToken)){
                                            Log.d("token", String.valueOf(tokenArray));
                                        }else{
                                            tokenArray.add(newToken);
                                            //update old and new token id into customer database
                                            updateToken(id,tokenArray);
                                        }


                                    }
                                }

                            }
                        });
                    }
                });

        //open home fragment on first launch
        HomeFragment homeFragment=new HomeFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout,homeFragment).commit();

    }


    public void changeMenu(int menuId){
        bottomNavigationView.setSelectedItemId(menuId);
    }

/*
    public void changeFragment(int fragmentId){

    }
*/


    public void updateToken(String documentID, final ArrayList<String> token){
        FirebaseFirestore firebaseFirestore=FirebaseFirestore.getInstance();
        firebaseFirestore.collection("customers")
                .document(documentID)
                .update("fcmToken",token)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d("FCM", String.valueOf(token));
                        }
                    }
                });

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                HomeFragment homeFragment=new HomeFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout,homeFragment).commit();
                break;
            case R.id.nav_products:
                StylistFragment stylistsFragment=new StylistFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction1=getSupportFragmentManager().beginTransaction();
                fragmentTransaction1.replace(R.id.frameLayout,stylistsFragment).commit();
                break;
            case R.id.nav_promotions:
                MyapptsFragment myaaptsFragment=new MyapptsFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction2=getSupportFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.frameLayout,myaaptsFragment).commit();
                break;

            case R.id.nav_purchases:
                HairtipsFragment hairtipsFragment=new HairtipsFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction3=getSupportFragmentManager().beginTransaction();
                fragmentTransaction3.replace(R.id.frameLayout,hairtipsFragment).commit();
                break;

            case R.id.nav_settings:
                AccountFragment accountFragment=new AccountFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction4=getSupportFragmentManager().beginTransaction();
                fragmentTransaction4.replace(R.id.frameLayout,accountFragment).commit();
                 break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
