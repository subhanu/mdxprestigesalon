package FirstFlowFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;


import com.example.dis015.mdxprestigesalon5.R;

import ModelClass.WebviewClientImpl;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyFragment extends Fragment {

    WebView webView;
    String url="https://mdxlegal2018.firebaseapp.com/legal_dk_customer.html";

    public PrivacyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_privacy, container, false);
        webView = view.findViewById(R.id.webview);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebviewClientImpl());

        return view;
    }

}
