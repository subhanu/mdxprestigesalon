package FirstFlowFragments;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dis015.mdxprestigesalon5.PhoneNumberLogin;
import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;


import ModelClass.Utility;
import alarmNotification.ServiceReceiver;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {


    private FirebaseAuth mAuth;
    TextView username,mobileno,gender,dob_Edt,deleteAccountTv,privacyTv,alertTv;
    ImageView profilePicture,editPicture,alertImg;
    int count=0;
    String id,strImageUrl;
    String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Button yesBtn,cancelBtn;
    private Uri imageUri;
    private StorageReference storageReference;
    AlarmManager manager;
    ImageView backImg;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_account, container, false);
        try {
            Button logout;
            username=view.findViewById(R.id.emailEdt);
            mobileno=view.findViewById(R.id.mobileNoEdt);
            gender=view.findViewById(R.id.genderEdt);
            dob_Edt=view.findViewById(R.id.ageEdt);
            deleteAccountTv=view.findViewById(R.id.deleteAccount);
            profilePicture=view.findViewById(R.id.imageView);
            editPicture=view.findViewById(R.id.editImg);
            alertImg=view.findViewById(R.id.alertImg);
            privacyTv=view.findViewById(R.id.privacyTv);
            alertTv=view.findViewById(R.id.timeReminingTv);

            backImg=view.findViewById(R.id.back_Img);


            mAuth = FirebaseAuth.getInstance();
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            storageReference= firebaseStorage.getReference();

            customerDetails();

            backImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeFragment homeFragment = new HomeFragment();
                    getFragmentManager().beginTransaction().replace(R.id.frameLayout, homeFragment).addToBackStack(null).commit();

                }
            });


            logout = (Button) view.findViewById(R.id.logoutBtn);
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        mAuth.signOut();
                        updateUI();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            privacyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PrivacyFragment timeDateFragment3=new PrivacyFragment();
                    getFragmentManager().beginTransaction().replace(R.id.frameLayout,timeDateFragment3).addToBackStack("").commit();

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    private void customerDetails() {
        final FirebaseFirestore firebaseFirestore= FirebaseFirestore.getInstance();
        final String auth=mAuth.getUid();

        firebaseFirestore.collection("customers").whereEqualTo("uid",auth).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot documentSnapshot:task.getResult()){
                        Log.d("id",documentSnapshot.getId());
                        count++;
                        if(documentSnapshot.exists()){
                            try{
                                id=documentSnapshot.getId();
                                String strFirstname=documentSnapshot.getString("firstName");
                                String strLastname=documentSnapshot.getString("lastName");
                                String strGender=documentSnapshot.getString("gender");
                                String strImageUrl=documentSnapshot.getString("imageUrl");
                                Date dob=documentSnapshot.getDate("dob");
                                String strMobile=documentSnapshot.getString("phone");

                                String strDate=dob.toString();
                                String strDob=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","yyyy-MM-dd",strDate);

                                Picasso.get().load(strImageUrl).into(profilePicture);
                                String name=strFirstname.concat(" ").concat(strLastname);
                                username.setText(name);
                                mobileno.setText(strMobile);
                                gender.setText(strGender);
                                dob_Edt.setText(strDob);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }else{
                            updateUI();
                        }
                    }
                }
            }
        });
        deleteAccountTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAlert();
            }
        });
        editPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        alertImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertNotification();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        try {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) {
                updateUI();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void updateUI() {
        try {
            Intent intent = new Intent(getContext(), PhoneNumberLogin.class);
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;
    }


    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask="Take Photo";
                    if(result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri=data.getData();

        upload();
        profilePicture.setImageBitmap(thumbnail);
    }


    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageUri=data.getData();
        upload();
        profilePicture.setImageBitmap(bm);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }
    private void deleteAlert(){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.delete_popup);
        dialog.show();
        yesBtn=dialog.findViewById(R.id.yesBtn);
        cancelBtn=dialog.findViewById(R.id.cancelBtn);
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseFirestore firebaseFirestore= FirebaseFirestore.getInstance();
                firebaseFirestore.collection("Customers").document(id).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mAuth.signOut();
                        updateUI();
                    }
                });
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


       /* AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
        builder.setTitle("Delete Account");
        builder.setMessage("Do you want to delete your account permanently");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore firebaseFirestore=FirebaseFirestore.getInstance();
                firebaseFirestore.collection("Customers").document(id).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mAuth.signOut();
                        updateUI();
//                        deleteUser();
                    }
                });


            }
        });
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();*/
       /* CustomDialog cdd=new CustomDialog(getActivity());
        cdd.show();
*/
    }

    public void updateProfilePic(final String imageUrl){
        FirebaseFirestore firebaseFirestore= FirebaseFirestore.getInstance();
        firebaseFirestore.collection("customers")
                .document(id)
                .update("imageUrl",imageUrl)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d("imageUrl", imageUrl);
                        }
                    }
                });
    }

    public void upload(){
        if(imageUri!=null){
            final StorageReference reference=storageReference.child("customers/"+ UUID.randomUUID().toString());
            reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Log.e("imageurl", "uri: " + uri.toString());
                            strImageUrl=uri.toString();
                            updateProfilePic(strImageUrl);
                            //Handle whatever you're going to do with the URL here
                        }
                    });
//                    Toast.makeText(getApplicationContext(),"File Uploaded",Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                    double totalProgress=(100*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();

//                    Toast.makeText(getApplicationContext(),"File uploaded"+(int)totalProgress,Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public void alertNotification(){

        final CharSequence[] items = { "No alert", "15 mins before",
                "30 mins before","1 hour before","2 hours before","Custom duration" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Create alert!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getActivity());
                if (items[item].equals("No alert")) {
                    userChoosenTask="No alert";
                    alertTv.setText(userChoosenTask);
                    dialog.dismiss();

                } else if (items[item].equals("15 mins before")) {
                    userChoosenTask="15 mins before";
                    alertTv.setText(userChoosenTask);

                    String time="00:15:00";
                    getBookings(time);


                } else if (items[item].equals("30 mins before")) {
                    userChoosenTask="30 mins before";
                    alertTv.setText(userChoosenTask);

                    String time="00:30:00";
                    getBookings(time);
                }else if(items[item].equals("1 hour before")){
                    userChoosenTask="1 hour before";
                    alertTv.setText(userChoosenTask);

                    String time="01:00:00";
                    getBookings(time);

                }else if(items[item].equals("2 hour before")){
                    userChoosenTask="2 hour before";
                    alertTv.setText(userChoosenTask);
                    String time="02:00:00";
                    getBookings(time);

                }else if(items[item].equals("Custom duration")){
                    userChoosenTask="Custom duration";
                    alertTv.setText(userChoosenTask);


                }
            }
        });
        builder.show();
    }

    public void convertIntoSeconds( String time){
//       String time = "02:30"; //mm:ss
        String[] units = time.split(":"); //will break the string up into an array
        int hours= Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]); //first element
//        int seconds = Integer.parseInt(units[2]); //second element
//        int duration = 60 * minutes + seconds+3600*hours; //add up our values
        int duration=60*minutes+3600*hours;
        Log.d("seconds", String.valueOf(duration));
        startService(duration);

    }
    public void getBookings(final String duration){
        FirebaseFirestore firebaseFirestore= FirebaseFirestore.getInstance();
        String strUid=mAuth.getUid();
        firebaseFirestore.collection("bookings").whereEqualTo("customer",strUid).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for (DocumentSnapshot documentSnapshot:task.getResult()){
                        String startTime=documentSnapshot.getString("startTime");
                        String strOnehour="60:00";
                        SimpleDateFormat simpledateFormat = new SimpleDateFormat("HH:mm");
                        simpledateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        try {
                            Date date1 = simpledateFormat.parse(duration);
                            Date date2 = simpledateFormat.parse(startTime);
                            long sum = date1.getTime()- date2.getTime();
                            String strCalculatedTime = simpledateFormat.format(new Date(sum));
                            convertIntoSeconds(strCalculatedTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

    }

    public void startService(int interval) {
        Intent alarmIntent = new Intent(getContext(), ServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, alarmIntent, 0);
        manager = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        manager = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, interval, pendingIntent);

    }
}
