package ModelClass;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

public class BookingsCollection implements Parcelable {

    private String bookingBy;
    private String bookingType;
    private String comments;
    private String customer;
    private String date;
    private Integer duration;
    private String endTime;
    private String imageUrl;
    private Integer price;
    private HashMap<String, String> recurring;
    private String recurringId;
    private ArrayList<String> services;
    private String startTime;
    private String stylist;

    public BookingsCollection(String bookingBy, String bookingType, String comments, String customer, String date, Integer duration, String endTime, String imageUrl, Integer price, HashMap<String, String> recurring, String recurringId, ArrayList<String> services, String startTime, String stylist) {
        this.bookingBy = bookingBy;
        this.bookingType = bookingType;
        this.comments = comments;
        this.customer = customer;
        this.date = date;
        this.duration = duration;
        this.endTime = endTime;
        this.imageUrl = imageUrl;
        this.price = price;
        this.recurring = recurring;
        this.recurringId = recurringId;
        this.services = services;
        this.startTime = startTime;
        this.stylist = stylist;
    }

    protected BookingsCollection(Parcel in) {
        bookingBy = in.readString();
        bookingType = in.readString();
        comments = in.readString();
        customer = in.readString();
        date = in.readString();
        if (in.readByte() == 0) {
            duration = null;
        } else {
            duration = in.readInt();
        }
        endTime = in.readString();
        imageUrl = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
        recurringId = in.readString();
        services = in.createStringArrayList();
        startTime = in.readString();
        stylist = in.readString();
    }

    public static final Creator<BookingsCollection> CREATOR = new Creator<BookingsCollection>() {
        @Override
        public BookingsCollection createFromParcel(Parcel in) {
            return new BookingsCollection(in);
        }

        @Override
        public BookingsCollection[] newArray(int size) {
            return new BookingsCollection[size];
        }
    };

    public String getBookingBy() {
        return bookingBy;
    }

    public void setBookingBy(String bookingBy) {
        this.bookingBy = bookingBy;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public HashMap<String, String> getRecurring() {
        return recurring;
    }

    public void setRecurring(HashMap<String, String> recurring) {
        this.recurring = recurring;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }

    public ArrayList<String> getServices() {
        return services;
    }

    public void setServices(ArrayList<String> services) {
        this.services = services;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStylist() {
        return stylist;
    }

    public void setStylist(String stylist) {
        this.stylist = stylist;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bookingBy);
        dest.writeString(bookingType);
        dest.writeString(comments);
        dest.writeString(customer);
        dest.writeString(date);
        if (duration == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(duration);
        }
        dest.writeString(endTime);
        dest.writeString(imageUrl);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(price);
        }
        dest.writeString(recurringId);
        dest.writeStringList(services);
        dest.writeString(startTime);
        dest.writeString(stylist);
    }
}

