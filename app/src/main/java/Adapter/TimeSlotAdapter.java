package Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.dis015.mdxprestigesalon5.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static FirstFlowFragments.BookAppointmentFragment.formateDateFromstring;


public class TimeSlotAdapter  extends RecyclerView.Adapter {

    private LayoutInflater mInflater;
    private String strSelectedDate;
    private List<Boolean> numberList;
    private List<String> mData;
    private TextView timetv;
    private List<RelativeLayout> activeTimeSlotsArray= new ArrayList<>();
    private List<RadioButton> radioButtonArray= new ArrayList<>();
    private TextView time;


    // data is passed into the constructor
    public TimeSlotAdapter(Context context, List<String> data,
                           String strSelectedDate, TextView timetv,
                           List<Boolean> numberList, TextView time) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.strSelectedDate = strSelectedDate;
        this.timetv=timetv;
        this.numberList=numberList;
        this.time=time;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        return new listViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
       if(position==5||position==11){
          /* RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
           params.setMargins(0,0,0,100);
           ((listViewHolder)holder).timeslot.setLayoutParams(params);*/

           RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ((listViewHolder)holder).timeslot.getLayoutParams();
           params.setMargins(0,0,0,50);
           ((listViewHolder)holder).timeslot.setLayoutParams(params);

       }

        // to get current date
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        Log.d("date", thisDate);

        if (thisDate.equals(strSelectedDate)) {
            // to get current time
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            Date currentLocalTime = cal.getTime();
            String strTime = currentLocalTime.toString();
            String strCurrenttime;
            strCurrenttime = formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy", "HH", strTime);

            if (strCurrenttime.contains("00")) {
                strCurrenttime = formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy", "hh", strTime);
            }
            String strCurrentmin = formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy", "mm", strTime);

            Integer currentHour = Integer.parseInt(strCurrenttime);
            Integer currentMin = Integer.parseInt(strCurrentmin);

            String strCurrentTimeMin;

            if (currentMin < 30) {
                currentMin=0;
                strCurrentTimeMin=formateDateFromstring("HH", "HH:mm", strCurrenttime);

            } else {
                currentMin = 30;
                String strmin = currentMin.toString();
                strCurrentTimeMin = strCurrenttime.concat(":").concat(strmin);

            }

            if (mData.get(position).equals("")) {
                numberList.set(position, true);

                ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.inactive_slots);
                ((listViewHolder) holder).radioButton.setClickable(false);
            } else {
                String freeBusyHour = formateDateFromstring("HH:mm", "HH", mData.get(position));
                String freeBusyMin = formateDateFromstring("HH:mm", "mm", mData.get(position));

                Integer hour = Integer.parseInt(freeBusyHour);
                Integer min = Integer.parseInt(freeBusyMin);

                if (hour < currentHour) {
                    numberList.set(position, true);
                    ((listViewHolder)holder).radioButton.setTextColor(R.color.inactiveslots);

                    ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.inactive_slots);
                    ((listViewHolder) holder).radioButton.setClickable(false);
                } else {
                    ((listViewHolder) holder).radioButton.setTextColor(Color.WHITE);
                    ((listViewHolder) holder).radioButton.setText(mData.get(position));
                    ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.active_slots_button);
                }
/*
                if (strCurrentTimeMin.equals(mData.get(position))) {
                    ((listViewHolder) holder).radioButton.setText(mData.get(position));
                    ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.active_slots);
                }
*/

              if(currentHour==hour && currentMin>min ){
                  numberList.set(position, true);
                  ((listViewHolder) holder).radioButton.setClickable(false);
                  ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.inactive_slots);

              }

            }
        } else {
            if (mData.get(position).equals("")) {
                ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.inactive_slots);
            } else {
                ((listViewHolder) holder).radioButton.setText(mData.get(position));
                ((listViewHolder) holder).radioButton.setTextColor(Color.WHITE);
                ((listViewHolder) holder).timeslot.setBackgroundResource(R.drawable.active_slots_button);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RadioButton radioButton;
        RelativeLayout timeslot;
        String strStartTime;
        String rs="K1";
        int selectedPosition;

        listViewHolder(View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radioBtton);
            timeslot = itemView.findViewById(R.id.mslot1);
            timeslot.setOnClickListener(this);
            radioButton.setOnClickListener(this);
        }


        public void onClick(View view) {
            selectedPosition=getAdapterPosition();
            if(numberList.get(getAdapterPosition()).equals(true)){
                radioButton.setClickable(false);
                Toast.makeText(itemView.getContext(),"Grey timeslot's are already booked please choose some other timeslot", Toast.LENGTH_LONG).show();
            }else {
                    if (activeTimeSlotsArray.size() >= 1) {
                        timeslot.setBackgroundResource(R.drawable.selected_slots_button);
                        strStartTime = radioButton.getText().toString();
                        time.setVisibility(View.VISIBLE);
                        timetv.setText(strStartTime);
                        for (int i = 0; i < activeTimeSlotsArray.size(); i++) {
                            activeTimeSlotsArray.get(i).setBackgroundResource(R.drawable.active_slots_button);
                            radioButtonArray.get(i).setChecked(false);
                            activeTimeSlotsArray.clear();
                            radioButtonArray.clear();
                        }
                        activeTimeSlotsArray.add(timeslot);
                        radioButtonArray.add(radioButton);
                    } else {
                        activeTimeSlotsArray.add(timeslot);
                        radioButtonArray.add(radioButton);
                        timeslot.setBackgroundResource(R.drawable.selected_slots_button);
                        strStartTime = radioButton.getText().toString();
                        time.setVisibility(View.VISIBLE);
                        timetv.setText(strStartTime);
                    }


            }
        }
    }


}
