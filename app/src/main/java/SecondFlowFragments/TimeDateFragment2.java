package SecondFlowFragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Adapter.TimeSlotAdapter;
import FirstFlowFragments.HomeFragment;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeDateFragment2 extends Fragment {

    TextView serviceNameTv,datePickerTv,time, timetv,dateTv;
    Button bookAppoinmentBtn;
    RelativeLayout datepicker;
    RecyclerView recyclerView;
    String strDate,strStartTime,strSelectedDate;
    TimeSlotAdapter timeSlotAdapter;
    ProgressBar progressBar;
    List<Boolean> numberList;
    ArrayList<String> timeList=new ArrayList<>();
    ImageView backImg;


    public TimeDateFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.book_appointments, container, false);
        View view = inflater.inflate(R.layout.nestedscroll, container, false);

        serviceNameTv = view.findViewById(R.id.serviceTv);
        datePickerTv = view.findViewById(R.id.datepickertv);
        datepicker = view.findViewById(R.id.datepickerLayout);
        dateTv=view.findViewById(R.id.dateTv);
        timetv = view.findViewById(R.id.timeTv);
        time=view.findViewById(R.id.time);
        bookAppoinmentBtn = view.findViewById(R.id.bookAppoinment);
        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar=view.findViewById(R.id.progressBar);
        backImg=view.findViewById(R.id.back_Img);


        numberList=new ArrayList<>();
        for(int i=0;i<=17;i++){
            numberList.add(false);
        }

        timeList.add("09:00");
        timeList.add("09:30");
        timeList.add("10:00");
        timeList.add("10:30");
        timeList.add("11:00");
        timeList.add("11:30");
        timeList.add("12:00");
        timeList.add("12:30");
        timeList.add("13:00");
        timeList.add("13:30");
        timeList.add("14:00");
        timeList.add("14:30");
        timeList.add("15:00");
        timeList.add("15:30");
        timeList.add("16:00");
        timeList.add("16:30");
        timeList.add("17:00");
        timeList.add("17:30");


        selectDate();

        return view;
    }

    public void selectDate(){
       Calendar myCalendar = Calendar.getInstance();

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                datepicker.setBackgroundResource(R.drawable.green_button);
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePicker = new DatePickerDialog(getContext(),R.style.DateTheme,new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1)
                                + "/" + String.valueOf(year);
                        String strdate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                        strSelectedDate=formateDateFromstring("dd/M/yyyy","dd/MM/yyyy",date);

                        String dateFormat=formateDateFromstring("yyyy-MM-dd","EE dd, MMM yyyy",strdate);
                        strDate=dateFormat;
                        datePickerTv.setText(dateFormat);
                        datePickerTv.setTextColor(Color.WHITE);
                        dateTv.setText(dateFormat);


                        // gridlayout to populate timeslots
                        int numberOfColumns = 3;
                        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
                        timeSlotAdapter = new TimeSlotAdapter(getContext(),timeList,strSelectedDate,timetv,numberList, time);
                        recyclerView.setAdapter(timeSlotAdapter);
                        progressBar.setVisibility(View.GONE);

                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePicker.show();

                datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            // Do Stuff
                            datepicker.setBackgroundResource(R.drawable.lightgrey_background);

                        }
                    }
                });
            }
        });
        bookAppoinmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    strStartTime=timetv.getText().toString();
                    ServicesFragment3 servicesFragment3 = new ServicesFragment3();
                    Bundle bundle=new Bundle();
                    bundle.putString("time",strStartTime);
                    bundle.putString("date",strDate);
                    servicesFragment3.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.frameLayout, servicesFragment3).addToBackStack(null).commit();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeFragment = new HomeFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, homeFragment).addToBackStack(null).commit();

            }
        });


    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;

    }

}
