package ModelClass;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebviewClientImpl extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        webView.loadUrl(url);
        return true;
    }

}

