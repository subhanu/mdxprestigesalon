package alarmNotification;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.example.dis015.mdxprestigesalon5.R;


public class ServiceReceiver extends BroadcastReceiver {
    private Context mcontext;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.mcontext = context;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logob_icon_prestigesalon)
                .setContentTitle("ABC")
                .setContentText("time to go")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());


        Intent alarmIntent = new Intent(mcontext, ServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mcontext, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) mcontext.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }
}
