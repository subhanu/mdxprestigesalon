package FirstFlowFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import Adapter.ServicesCategoryAdapter;
import SecondFlowFragments.TimeDateFragment2;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnMapReadyCallback {
    SupportMapFragment supportMapFragment;
    RecyclerView recyclerView;
    LinearLayout menLayout, womenLayout, childrenLayout;
    Button navigateToGoogle, bookAppointmentBtn,callButon;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        bookAppointmentBtn = view.findViewById(R.id.bookAppointmentBtn);
        recyclerView = view.findViewById(R.id.serviceList);
        navigateToGoogle = view.findViewById(R.id.navigateToBtn);
        menLayout = view.findViewById(R.id.menLayout);
        womenLayout = view.findViewById(R.id.womenLayout);
        childrenLayout = view.findViewById(R.id.childrenLayout);
        callButon=view.findViewById(R.id.ContactNoBtn);



        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (supportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            supportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, supportMapFragment).commit();
        }

        //by default men's services should be selected and visible
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    menLayout.setBackgroundResource(R.drawable.grey_background);
                    ServicesCategoryAdapter servicesAdapter2 = new ServicesCategoryAdapter(task);
                    recyclerView.setAdapter(servicesAdapter2);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                }
            }
        });
        onClickListener();
        supportMapFragment.getMapAsync(this);

        return view;
    }

    public void onClickListener() {
        bookAppointmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* TimeServiceStylistFragment timeServiceStylistFragment=new TimeServiceStylistFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout,timeServiceStylistFragment).addToBackStack("").commit();*/
                TimeDateFragment2 timeDateFragment3=new TimeDateFragment2();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout,timeDateFragment3).addToBackStack("").commit();

            }
        });

        callButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number="";
                String strButtonText=callButon.getText().toString();
                String strNumber=strButtonText.replace("ring","");
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + strNumber));
                startActivity(intent);
            }
        });

        navigateToGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String uri="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyCrOQNVNM8PwLFwb4h7Dr-DOf3u5foMRC4&center=-33.9,151.14999999999998&zoom=12&format=png&maptype=roadmap&size=480x360";
//               String uri="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyCrOQNVNM8PwLFwb4h7Dr-DOf3u5foMRC4&center=-33.9,151.14999999999998&zoom=12&format=png&maptype=roadmap&style=element:geometry%7Ccolor:0x1d2c4d&style=element:labels.text.fill%7Ccolor:0x8ec3b9&style=element:labels.text.stroke%7Ccolor:0x1a3646&style=feature:administrative.country%7Celement:geometry.stroke%7Ccolor:0x4b6878&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0x64779e&style=feature:administrative.province%7Celement:geometry.stroke%7Ccolor:0x4b6878&style=feature:landscape.man_made%7Celement:geometry.stroke%7Ccolor:0x334e87&style=feature:landscape.natural%7Celement:geometry%7Ccolor:0x023e58&style=feature:poi%7Celement:geometry%7Ccolor:0x283d6a&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x6f9ba5&style=feature:poi%7Celement:labels.text.stroke%7Ccolor:0x1d2c4d&style=feature:poi.park%7Celement:geometry.fill%7Ccolor:0x023e58&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x3C7680&style=feature:road%7Celement:geometry%7Ccolor:0x304a7d&style=feature:road%7Celement:labels.text.fill%7Ccolor:0x98a5be&style=feature:road%7Celement:labels.text.stroke%7Ccolor:0x1d2c4d&style=feature:road.highway%7Celement:geometry%7Ccolor:0x2c6675&style=feature:road.highway%7Celement:geometry.stroke%7Ccolor:0x255763&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0xb0d5ce&style=feature:road.highway%7Celement:labels.text.stroke%7Ccolor:0x023e58&style=feature:transit%7Celement:labels.text.fill%7Ccolor:0x98a5be&style=feature:transit%7Celement:labels.text.stroke%7Ccolor:0x1d2c4d&style=feature:transit.line%7Celement:geometry.fill%7Ccolor:0x283d6a&style=feature:transit.station%7Celement:geometry%7Ccolor:0x3a4762&style=feature:water%7Celement:geometry%7Ccolor:0x0e1626&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x4e6d70&size=480x360";
                String uri = "https://www.google.com/maps/place/Alsgade+3,+6400+S%C3%B8nderborg,+Denmark/@54.91434,9.7947752,17z/data=!3m1!4b1!4m5!3m4!1s0x47b339e0d140e3c7:0x859af00497e13449!8m2!3d54.91434!4d9.7969639";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);

            }
        });
        menLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menLayout.setBackgroundResource(R.drawable.grey_background);
                childrenLayout.setBackgroundResource(R.color.white);
                womenLayout.setBackgroundResource(R.color.white);

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            ServicesCategoryAdapter servicesAdapter2=new ServicesCategoryAdapter(task);
                            recyclerView.setAdapter(servicesAdapter2);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(layoutManager);
                        }
                    }
                });

            }
        });

        womenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                womenLayout.setBackgroundResource(R.drawable.grey_background);
                childrenLayout.setBackgroundResource(R.color.white);
                menLayout.setBackgroundResource(R.color.white);
                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("services_women").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            ServicesCategoryAdapter servicesAdapter2=new ServicesCategoryAdapter(task);
                            recyclerView.setAdapter(servicesAdapter2);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(layoutManager);
                        }
                    }
                });
            }
        });
        childrenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                childrenLayout.setBackgroundResource(R.drawable.grey_background);
                menLayout.setBackgroundResource(R.color.white);
                womenLayout.setBackgroundResource(R.color.white);
                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("services_children").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            ServicesCategoryAdapter servicesAdapter2=new ServicesCategoryAdapter(task);
                            recyclerView.setAdapter(servicesAdapter2);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(layoutManager);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException se) {

        }

       /* boolean success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        getContext(), R.raw.stylist_json));

        if (!success) {
            Log.e(TAG, "Style parsing failed.");
        }*/

        //Edit the following as per you needs
        googleMap.setTrafficEnabled(true);
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
//        googleMap.getUiSettings().setZoomControlsEnabled(true);


        LatLng placeLocation = new LatLng(54.9025813,9.8043215); //Make them global
        Marker placeMarker = googleMap.addMarker(new MarkerOptions().position(placeLocation)
                .title("Alsgade"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(placeLocation));

        //zoom google map based on you requirements
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);

    }
}
