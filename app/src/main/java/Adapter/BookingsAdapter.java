package Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import FirstFlowFragments.StylistFragment;
import ModelClass.BookingsCollection;
import ModelClass.ServicesCollection;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

public class BookingsAdapter extends RecyclerSwipeAdapter<BookingsAdapter.listViewHolder> {
    private FirebaseFirestore firebaseFirestore;
    private List<BookingsCollection> bookingsCollectionList;
    private ArrayList<String> servicesArray=new ArrayList<>();
    private ArrayList<String> appointmentList;
    private SharedPreferences sharedPreferences;
    private String strDate,strBookingDate,strBookingMonth,strBookingYear,strBookingHour;
    View view;
    private Context context;

    private List<ServicesCollection> servicesCollectionList;

    public BookingsAdapter(Context context, List<BookingsCollection> bookingsCollectionList,
                           List<ServicesCollection> servicesCollectionList, ArrayList<String> appointmentList) {
        this.context=context;
        this.bookingsCollectionList=bookingsCollectionList;
        this.servicesCollectionList=servicesCollectionList;
        this.appointmentList=appointmentList;
    }


    @Override
    public listViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        firebaseFirestore=FirebaseFirestore.getInstance();
        view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_appointments_recycler_item,parent,false);
//        return new listViewHolder(view);


        return new listViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final listViewHolder viewHolder, final int position) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Date currentLocalTime = cal.getTime();
        String strTime = currentLocalTime.toString();

        String currentDate=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","dd",strTime);
        String currentMonth=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","MM",strTime);
        String currentYear=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","yyyy",strTime);
        String currentHour=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","HH",strTime);

            strDate=bookingsCollectionList.get(position).getDate();
            String startTime=bookingsCollectionList.get(position).getStartTime();
            strBookingHour=formateDateFromstring("HH:mm","HH",startTime);
            strBookingDate=formateDateFromstring("yyyy-MM-dd","dd",strDate);
            strBookingMonth=formateDateFromstring("yyyy-MM-dd","MM",strDate);
            strBookingYear=formateDateFromstring("yyyy-MM-dd","yyyy",strDate);


        //convert bookings date,year,month into integer to compare with current date
        int intBookingYear=Integer.parseInt(strBookingYear);
        int intBookingMonth=Integer.parseInt(strBookingMonth);
        int intBookingDate=Integer.parseInt(strBookingDate);
        int intBookingHour=Integer.parseInt(strBookingHour);

        int intCurrentYear=Integer.parseInt(currentYear);
        int intCurrentMonth=Integer.parseInt(currentMonth);
        int intCurrentDate=Integer.parseInt(currentDate);
        int intCurrentHour=Integer.parseInt(currentHour);

        if(intBookingYear<intCurrentYear){
            viewHolder.cardview.setBackgroundResource(R.drawable.card_upcoming_bg);
        }else if(intBookingYear==intCurrentYear){
            if(intBookingMonth<intCurrentMonth){
                viewHolder.cardview.setBackgroundResource(R.drawable.card_upcoming_bg);

            }else if(intBookingMonth==intCurrentMonth) {
                if(intBookingDate<intCurrentDate){
                    viewHolder.cardview.setBackgroundResource(R.drawable.card_upcoming_bg);

                }else if(intBookingDate>intCurrentDate){
                    viewHolder.cardview.setBackgroundResource(R.drawable.active_appointments);


                }else if(intBookingDate==intCurrentDate){
                    if(intBookingHour<intCurrentHour){
                        viewHolder.cardview.setBackgroundResource(R.drawable.card_upcoming_bg);
                    }
                }
            }else if(intBookingMonth>intCurrentMonth){
                viewHolder.cardview.setBackgroundResource(R.drawable.active_appointments);

            }
        }else if(intBookingYear>intCurrentYear){
            viewHolder.cardview.setBackgroundResource(R.drawable.card_upcoming_bg);
        }



        try{
            String dateFormat=formateDateFromstring("yyyy-MM-dd","EE dd, MMM yyyy",bookingsCollectionList.get(position).getDate());

            viewHolder.timeTV.setText(bookingsCollectionList.get(position).getStartTime());
            viewHolder.dateTv.setText(dateFormat);

        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            for(int i=0;i<bookingsCollectionList.size();i++){
                for(int j=0;j<bookingsCollectionList.get(position).getServices().size();j++){
                    for(int k=0;k<servicesCollectionList.size();k++){
                        if(bookingsCollectionList.get(position).getServices().get(j).contains(servicesCollectionList.get(k).getId())){
                            String service=servicesCollectionList.get(k).getName();
                            servicesArray.add(service);

                        }
                    }
                }
                if(servicesArray.size()==2){
                    viewHolder.serviceTv.setText(servicesArray.get(0)+" "+"+"+"\n"+servicesArray.get(1));
                }else{
                    viewHolder.serviceTv.setText(servicesArray.get(0));
                }
                servicesArray.clear();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        Picasso.get().load(bookingsCollectionList.get(position).getImageUrl()).into((viewHolder.stylistImg));

        try{
            firebaseFirestore.collection("stylists").document(bookingsCollectionList.get(position).getStylist()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()){
                        try {
                            String stylistname = task.getResult().getString("firstName");
                            String lastname=task.getResult().getString("lastName");
                            viewHolder.stylistNameTv.setText(stylistname);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        viewHolder.Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String bookingId=appointmentList.get(position);
                sharedPreferences= PreferenceManager.getDefaultSharedPreferences(view.getContext());
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("appointmentid",bookingId);
                editor.commit();

                StylistFragment stylistFragment = new StylistFragment();
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,stylistFragment).addToBackStack(null).commit();
            }
        });

        viewHolder.Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String documentId=appointmentList.get(position);
                firebaseFirestore.collection("bookings")
                        .document(documentId)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
//                                viewHolder.viewForeground.removeAllViews();
                                removeAt(position);

                            }
                        });
            }
        });


    }

    public void removeAt(int position) {
        bookingsCollectionList.remove(position);
        appointmentList.remove(position);
        notifyDataSetChanged();
        notifyItemRemoved(position);
    }


    @Override
    public int getItemCount() {
        return bookingsCollectionList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }

    public class listViewHolder extends RecyclerView.ViewHolder{
        private TextView serviceTv,timeTV,dateTv,stylistNameTv,lastname;
        private CircleImageView stylistImg,cardview;
        RelativeLayout Delete,Edit;
        SwipeLayout swipeLayout;
        CardView viewForeground;

        private listViewHolder(final View itemView) {
            super(itemView);
            serviceTv =  itemView.findViewById(R.id.serviceTv);
            timeTV =  itemView.findViewById(R.id.timeTv);
            stylistNameTv = itemView.findViewById(R.id.stylistnameTV);
            dateTv =itemView.findViewById(R.id.dateTv);
            stylistImg =  itemView.findViewById(R.id.stylistImg);
            Edit =  itemView.findViewById(R.id.editImg);
            swipeLayout =  itemView.findViewById(R.id.swipe);
            Delete =  itemView.findViewById(R.id.deleteImg);
            lastname=itemView.findViewById(R.id.stylistLastnameTV);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            cardview=itemView.findViewById(R.id.cardImg);
        }

    }

    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;
    }


}
