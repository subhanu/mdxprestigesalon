package FirstFlowFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import com.example.dis015.mdxprestigesalon5.R;

import ModelClass.WebviewClientImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class HairtipsFragment extends Fragment {
    WebView webView;
    String url="https://www.facebook.com/prestigehairbeautysalon/";
    ImageView backImg;

    public HairtipsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_hairtips, container, false);
        webView=view.findViewById(R.id.webview);
        backImg=view.findViewById(R.id.back_Img);

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeFragment = new HomeFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, homeFragment).addToBackStack(null).commit();

            }
        });

        webView.loadUrl(url);

        webView.setWebViewClient(new WebviewClientImpl());

        return view;
    }
}
