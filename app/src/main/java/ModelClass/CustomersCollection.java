package ModelClass;



import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by DIS015 on 3/20/2018.
 */

public class CustomersCollection implements Parcelable {
    private String comments;
    private String customerType;
    private Date dob;
    private String firstName;
    private String imageUrl;
    private String gender;
    private String lastName;
    private String phone;
    private String uid;
    private ArrayList<String> fcmToken;

    public CustomersCollection(String comments, String customerType, Date dob, String firstName, String imageUrl, String gender, String lastName, String phone, String uid, ArrayList<String> fcmToken) {
        this.comments = comments;
        this.customerType = customerType;
        this.dob = dob;
        this.firstName = firstName;
        this.imageUrl = imageUrl;
        this.gender = gender;
        this.lastName = lastName;
        this.phone = phone;
        this.uid = uid;
        this.fcmToken = fcmToken;
    }

    protected CustomersCollection(Parcel in) {
        comments = in.readString();
        customerType = in.readString();
        firstName = in.readString();
        imageUrl = in.readString();
        gender = in.readString();
        lastName = in.readString();
        phone = in.readString();
        uid = in.readString();
        fcmToken = in.createStringArrayList();
    }

    public static final Creator<CustomersCollection> CREATOR = new Creator<CustomersCollection>() {
        @Override
        public CustomersCollection createFromParcel(Parcel in) {
            return new CustomersCollection(in);
        }

        @Override
        public CustomersCollection[] newArray(int size) {
            return new CustomersCollection[size];
        }
    };

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<String> getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(ArrayList<String> fcmToken) {
        this.fcmToken = fcmToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comments);
        dest.writeString(customerType);
        dest.writeString(firstName);
        dest.writeString(imageUrl);
        dest.writeString(gender);
        dest.writeString(lastName);
        dest.writeString(phone);
        dest.writeString(uid);
        dest.writeStringList(fcmToken);
    }
}
