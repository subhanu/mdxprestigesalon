package FirstFlowFragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.HomeActivity;
import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import Adapter.TimeSlotAdapter;
import ModelClass.BookingsCollection;
import ModelClass.HttpHandler;
import ModelClass.StylistDetails2;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookAppointmentFragment extends Fragment {

    TextView durationTv, serviceNameTv, stylistname, datePickerTv,time, timetv,dateTv;
    Button bookAppoinmentBtn,bookingPopup;
    CircleImageView stylistImgView;
    ImageView backImg;

    String strDate,endTime, strCustomer, strStylistid,documentId;
    private StylistDetails2 stylistDetails2;
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth firebaseAuth;
    BookingsCollection bookingsCollection;
    RelativeLayout datepickerLayout;
    Calendar myCalendar;
    DatePickerDialog datePickerDialog;
    String strStartDate,strSelectedDate;
    List<Boolean> numberList;
    List<String> timeSlotList;
    RecyclerView recyclerView;
    TimeSlotAdapter recyclerGridAdapter;
    ProgressBar progressBar;
    ArrayList<String> dateArray=new ArrayList<>();

    public BookAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.nestedscroll, container, false);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        numberList=new ArrayList<>();
        for(int i=0;i<=17;i++){
            numberList.add(false);
        }


        progressBar=view.findViewById(R.id.progressBar);
        durationTv =  view.findViewById(R.id.durationTv);
        serviceNameTv = view.findViewById(R.id.serviceTv);
        stylistname =  view.findViewById(R.id.stylistNameTv);
        bookAppoinmentBtn =  view.findViewById(R.id.bookAppoinment);
        backImg=view.findViewById(R.id.back_Img);

        timetv =  view.findViewById(R.id.timeTv);
        time=view.findViewById(R.id.time);
        dateTv=view.findViewById(R.id.dateTv);
        datePickerTv =view.findViewById(R.id.datepickertv);
        datepickerLayout =  view.findViewById(R.id.datepickerLayout);
        recyclerView = view.findViewById(R.id.recyclerView);
        stylistImgView =  view.findViewById(R.id.stylistImg);

        firebaseFirestore = FirebaseFirestore.getInstance();

        try {
            //geta data from data stylistDetails class
            Bundle bundle = getArguments();
            if (bundle != null) {
                stylistDetails2 = (StylistDetails2) bundle.getParcelable("stylistflow");
                strStylistid = stylistDetails2.getStylistid();
                documentId=stylistDetails2.getDocumentId();
            }
            stylistname.setText(stylistDetails2.getFirstname());
            Picasso.get().load(stylistDetails2.getImageUrl()).into(stylistImgView);

            ArrayList<String> serviceArray;
            serviceArray=stylistDetails2.getServices();

            if(serviceArray.size()==2){
                serviceNameTv.setText(serviceArray.get(0)+" "+"+"+"\n"+serviceArray.get(1));
            }else{
                serviceNameTv.setText(stylistDetails2.getServices().toString().replaceAll("\\[|\\]", ""));

            }
            strCustomer = firebaseAuth.getUid();

            details();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void details() {
        myCalendar = Calendar.getInstance();
        datepickerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                datepickerLayout.setBackgroundResource(R.drawable.green_button);

                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);


                datePickerDialog = new DatePickerDialog(getContext(),R.style.DateTheme,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strdate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                        String date=dayOfMonth+"/"+(monthOfYear+1)+"/"+year;

                        strSelectedDate=formateDateFromstring("dd/M/yyyy","dd/MM/yyyy",date);
                        String date_after = formateDateFromstring("yyyy-MM-dd", "yyyy-MM-dd", strdate);
                        String dateFormat=formateDateFromstring("yyyy-MM-dd","EE dd, MMM yyyy",strdate);

                        strDate=dateFormat;
                        datePickerTv.setText(dateFormat);
                        datePickerTv.setTextColor(Color.WHITE);
                        dateArray.add(strDate);

                        dateTv.setText(strDate);
                        strStartDate=date_after;
                        new GetContacts().execute();

//                        getJsonResponsePost(date_after);
                    }
                }, yy, mm, dd);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.show();

                datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            // Do Stuff
                            datepickerLayout.setBackgroundResource(R.drawable.lightgrey_background);

                        }
                    }
                });

            }

        });


        bookAppoinmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                showPopup();
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StylistFragment stylistFragment = new StylistFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, stylistFragment).addToBackStack(null).commit();

            }
        });


    }

    private void showPopup() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.confirm_booking);
        dialog.show();
        bookingPopup=dialog.findViewById(R.id.okBtn);
        bookingPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String time=timetv.getText().toString();

                String strDuration= Integer.toString(stylistDetails2.getDuration());
                String strServiceDuration=formateDateFromstring("mm","HH:mm",strDuration);
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date date1 = dateFormat.parse(time);
                    Date date2 = dateFormat.parse(strServiceDuration);
                    long sum = date1.getTime()+ date2.getTime();
                    endTime = dateFormat.format(new Date(sum));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    String strDateTime=strDate;
                    String strDate=formateDateFromstring("EE dd, MMM yyyy","yyyy-MM-dd",strDateTime);


                    HashMap<String, String> recurring;
                    recurring = new HashMap<>();
                    recurring.put("startDate",strDate);
                    recurring.put("endDate",strDate);
                    recurring.put("type","no");

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    Date currentLocalTime = cal.getTime();
                    String strTime = currentLocalTime.toString();
                    String strCurrenttime;
                    String strCurrentDate;
                    strCurrentDate=formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy","yyyMMdd",strTime);
                    String date=  strCurrentDate.concat("T");
                    strCurrenttime = formateDateFromstring("EEEE MMM dd hh:mm:ss Z yyyy", "HHmmss", strTime);
                   String strDatetime=date.concat(strCurrenttime);
                    String recurringId =strDatetime.concat(strStylistid);



                    bookingsCollection=new BookingsCollection("mobile",
                            "booking","",strCustomer,strDate,
                            stylistDetails2.getDuration(),endTime,
                            stylistDetails2.getImageUrl(), stylistDetails2.getPrice(),
                            recurring,recurringId, stylistDetails2.getServicesId(),
                            time,strStylistid);
                    Log.i("book", String.valueOf(bookingsCollection));
                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    if (documentId!= null && !documentId.equalsIgnoreCase("")) {
                        firebaseFirestore.collection("bookings").document(documentId)
                                .set(bookingsCollection)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dialog.dismiss();
                                        // change fragment when button is clicked
                                        MyapptsFragment myaaptsFragment = new MyapptsFragment();
                                        AppCompatActivity activity = (AppCompatActivity)getContext();
                                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, myaaptsFragment).addToBackStack(null).commit();

                                        // change navigation item color of a fragment when button is clicked
                                        ((HomeActivity)getActivity()).changeMenu(R.id.navigation_apps);
                                    }
                                });

                    } else{
                        firebaseFirestore.collection("bookings").add(bookingsCollection).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                dialog.dismiss();
                                MyapptsFragment myaaptsFragment = new MyapptsFragment();
                                AppCompatActivity activity = (AppCompatActivity)getContext();
                                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, myaaptsFragment).addToBackStack(null).commit();

                                // change navigation item color of a fragment when button is clicked
                                ((HomeActivity)getActivity()).changeMenu(R.id.navigation_apps);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed;
        String outputDate ="";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;

    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String currentStylistID=strStylistid;
            String duration=stylistDetails2.getDuration().toString();
            String url= "https://us-central1-mdxprestigesalon5.cloudfunctions.net/freeBusyFlow?date="+strStartDate+"&duration="+duration+"&currentStylistID="+currentStylistID;
            String jsonStr = sh.makeServiceCall(url);
            Log.e(TAG, "Response from url: " + jsonStr);
            if(jsonStr!=null){
                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);

                    String str1clk=jsonObj.getString("1");
                    String str2clk=jsonObj.getString("2");
                    String str3clk=jsonObj.getString("3");
                    String str4clk=jsonObj.getString("4");
                    String str5clk=jsonObj.getString("5");
                    String str6clk=jsonObj.getString("6");
                    String str7clk=jsonObj.getString("7");
                    String str8clk=jsonObj.getString("8");
                    String str9clk=jsonObj.getString("9");
                    String str10clk=jsonObj.getString("10");
                    String str11clk=jsonObj.getString("11");
                    String str12clk=jsonObj.getString("12");
                    String str13clk=jsonObj.getString("13");
                    String str14clk=jsonObj.getString("14");
                    String str15clk=jsonObj.getString("15");
                    String str16clk=jsonObj.getString("16");
                    String str17clk=jsonObj.getString("17");
                    String str18clk=jsonObj.getString("18");

                    timeSlotList=new ArrayList<>();
                    timeSlotList.add(str1clk);
                    timeSlotList.add(str2clk);
                    timeSlotList.add(str3clk);
                    timeSlotList.add(str4clk);
                    timeSlotList.add(str5clk);
                    timeSlotList.add(str6clk);
                    timeSlotList.add(str7clk);
                    timeSlotList.add(str8clk);
                    timeSlotList.add(str9clk);
                    timeSlotList.add(str10clk);
                    timeSlotList.add(str11clk);
                    timeSlotList.add(str12clk);
                    timeSlotList.add(str13clk);
                    timeSlotList.add(str14clk);
                    timeSlotList.add(str15clk);
                    timeSlotList.add(str16clk);
                    timeSlotList.add(str17clk);
                    timeSlotList.add(str18clk);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            int numberOfColumns = 3;
            if(dateArray.size()>=2){
                numberList.clear();;
                for(int i=0;i<=17;i++){
                    numberList.add(false);
                }
            }
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
            recyclerGridAdapter = new TimeSlotAdapter(getContext(), timeSlotList,strSelectedDate,timetv,numberList,time);
            recyclerView.setAdapter(recyclerGridAdapter);
            progressBar.setVisibility(View.GONE);
        }
    }

/*
    public void startService(int interval) {
        Intent alarmIntent = new Intent(getContext(), ServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        manager = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, interval, pendingIntent);

    }
*/

    public void convertMinToSeconds(String time){

        long min = Integer.parseInt(time.substring(0, 2));
        long sec = Integer.parseInt(time.substring(3));

        long t = (min * 60L) + sec;

        long result = TimeUnit.SECONDS.toMillis(t);
        Log.d("result", String.valueOf(result));

    }
}




