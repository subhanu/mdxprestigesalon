package ModelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ServiceDetailsCollection implements Parcelable {
    private String id;
    private String name;
    private Integer price;
    private ArrayList<String> durationArray;
    private ArrayList<String> stylistArray;

    public ServiceDetailsCollection(String id, String name, Integer price, ArrayList<String> durationArray, ArrayList<String> stylistArray) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.durationArray = durationArray;
        this.stylistArray = stylistArray;
    }

    protected ServiceDetailsCollection(Parcel in) {
        id = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
        durationArray = in.createStringArrayList();
        stylistArray = in.createStringArrayList();
    }

    public static final Creator<ServiceDetailsCollection> CREATOR = new Creator<ServiceDetailsCollection>() {
        @Override
        public ServiceDetailsCollection createFromParcel(Parcel in) {
            return new ServiceDetailsCollection(in);
        }

        @Override
        public ServiceDetailsCollection[] newArray(int size) {
            return new ServiceDetailsCollection[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ArrayList<String> getDurationArray() {
        return durationArray;
    }

    public void setDurationArray(ArrayList<String> durationArray) {
        this.durationArray = durationArray;
    }

    public ArrayList<String> getStylistArray() {
        return stylistArray;
    }

    public void setStylistArray(ArrayList<String> stylistArray) {
        this.stylistArray = stylistArray;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(price);
        }
        dest.writeStringList(durationArray);
        dest.writeStringList(stylistArray);
    }
}
