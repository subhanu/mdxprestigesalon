package com.example.dis015.mdxprestigesalon5;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class PhoneNumberLogin extends AppCompatActivity {
    EditText phoneNo_Edt;
    Button loginBtn;
    TextView incorrectTv;
    FirebaseAuth firebaseAuth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final String TAG = "FirebasePhoneNumAuth";
    PhoneAuthProvider.ForceResendingToken mResendToken;
    String mVerificationId;
    String number,strCountryCode,strNumber;
    CountryCodePicker countryCodePicker;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isNetworkAvailable()){
            checkInternetConnection();
        }
        setContentView(R.layout.activity_phoneno_login);

        phoneNo_Edt= findViewById(R.id.phoneNoEdt);
        incorrectTv =findViewById(R.id.icorrectTv);
        loginBtn=findViewById(R.id.phoneNoLoginBtn);
        countryCodePicker=findViewById(R.id.ccp);
        progressBar=findViewById(R.id.progressBar);


        phoneNo_Edt.setSingleLine();


        //default countrycode
        String countrySymbol="+";
        String countryCode=countryCodePicker.getDefaultCountryCode();
        strCountryCode=countrySymbol.concat(countryCode);
        firebaseAuth= FirebaseAuth.getInstance();

        addOnclickListener();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                progressBar.setVisibility(View.GONE);
                Intent intent=new Intent(PhoneNumberLogin.this,VerifyOtpActivity.class);
                intent.putExtra("verificationcode",mVerificationId);
                intent.putExtra("phonenumber",number);

                startActivity(intent);

                // ...
            }
        };


    }
    private boolean isNetworkAvailable() {
        // checks internet connection
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void checkInternetConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }



    public void addOnclickListener(){

        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                String countrySymbol="+";
                String countryCode=selectedCountry.getPhoneCode();
                strCountryCode=countrySymbol.concat(countryCode);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String strnumber=phoneNo_Edt.getText().toString();
                    number=strCountryCode.concat(strnumber);
                    if(number!=null && !number.equalsIgnoreCase("")){
                        send_sms();
                    }else {
                        Toast.makeText(getApplicationContext(),"please provide your number", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        if(firebaseAuth.getCurrentUser()!=null){
            updateUI();
        }

    }
    public void updateUI() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void send_sms(){
        progressBar.setVisibility(View.VISIBLE);
        String phoneNumber=phoneNo_Edt.getText().toString();
         strNumber=strCountryCode.concat(phoneNumber);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                strNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            checkUserExixt();
//                            FirebaseUser user = task.getResult().getUser();
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private void checkUserExixt() {
        final String uid = firebaseAuth.getUid();
        Log.d("mobilenumber",strNumber);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();

        firebaseFirestore.collection("customers").whereEqualTo("phone",strNumber).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    int count = 0;
                    for(QueryDocumentSnapshot document : task.getResult()){
                        Log.d("data" , document.getId() + " =>" + document.getData());
                        count++;
                    }
                    if(count>=1){
                        Log.d("strNumber",strNumber);
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }else if(count==0){
                        Log.d("strNumber",strNumber);
                        Intent intent = new Intent(getApplicationContext(), CustomerRegistration.class);
                        intent.putExtra("mobilenumber",number);
                        startActivity(intent);
                        finish();
                    }

                }
            }
        });
    }

}
