package Adapter;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import FirstFlowFragments.StylistFragment;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * Created by DIS015 on 3/9/2018.
 */

public class BookAppointmentAdapter extends RecyclerSwipeAdapter<BookAppointmentAdapter.listViewHolder> {
    private final Task<QuerySnapshot> task;
    private FirebaseFirestore firebaseFirestore;
    private ArrayList<String> servicesArray=new ArrayList<>();
    private ArrayList<String> serviceListArray;
    private ArrayList<String> documentIdArray;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> appointmentList;
    private String strStylistId;
    private SwipeRefreshLayout swipeRefreshLayout;

    public BookAppointmentAdapter(Task<QuerySnapshot> task, ArrayList<String> serviceListArray, ArrayList<String> documentIdArray, ArrayList<String> appointmentList, SwipeRefreshLayout swipeRefreshLayout) {
        this.task = task;
        this.documentIdArray=documentIdArray;
        this.serviceListArray=serviceListArray;
        this.appointmentList=appointmentList;
        this.swipeRefreshLayout=swipeRefreshLayout;

    }

    @Override
    public listViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_appointments_recycler_item,parent,false);
        firebaseFirestore = FirebaseFirestore.getInstance();
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final listViewHolder viewHolder, final int position) {
        strStylistId = task.getResult().getDocuments().get(position).getString("stylist");
        String startDatetimestamp = task.getResult().getDocuments().get(position).getString("startTime");
        String date=task.getResult().getDocuments().get(position).getString("date");
        Picasso.get().load(task.getResult().getDocuments().get(position).getString("imageUrl")).into(viewHolder.stylistImg);
        ArrayList<String> serviceDetails = (ArrayList<String>) task.getResult().getDocuments().get(position).get("services");

        try{
            String dateFormat=formateDateFromstring("yyyy-MM-dd","EE dd, MMM yyyy",date);

            viewHolder.timeTV.setText(startDatetimestamp);
            viewHolder.dateTv.setText(dateFormat);

        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            for(int i=0;i<serviceDetails.size();i++){
                for(int j=0;j<documentIdArray.size();j++){
                    if(serviceDetails.get(i).contains(documentIdArray.get(j))){
                        String service=serviceListArray.get(j);
                        servicesArray.add(service);
                        if(servicesArray.size()==2){
                            viewHolder.serviceTv.setText(servicesArray.get(0)+" "+"+"+"\n"+servicesArray.get(1));
                        }else{
                            viewHolder.serviceTv.setText(servicesArray.get(0));
                        } }
                }
            }
            servicesArray.clear();
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            firebaseFirestore.collection("stylists").document(strStylistId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()){
                        try {
                            String stylistname = task.getResult().getString("firstName");
                            String lastname=task.getResult().getString("lastName");
                            viewHolder.stylistNameTv.setText(stylistname);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
        viewHolder.Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String bookingId=appointmentList.get(position);
                sharedPreferences= PreferenceManager.getDefaultSharedPreferences(view.getContext());
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("appointmentid",bookingId);
                editor.commit();

                StylistFragment stylistFragment = new StylistFragment();
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout,stylistFragment).addToBackStack(null).commit();
            }
        });

        viewHolder.Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String documentId=appointmentList.get(position);
                firebaseFirestore.collection("bookings")
                        .document(documentId)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                viewHolder.viewForeground.removeAllViews();
                                removeAt(position);

                            }
                        });
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return task.getResult().getDocuments().size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void removeAt(int position) {
       notifyItemRemoved(position);
    }


    public class listViewHolder extends RecyclerView.ViewHolder{
        private TextView serviceTv,timeTV,dateTv,stylistNameTv,lastname;
        private CircleImageView stylistImg;
        RelativeLayout Delete,Edit;
        SwipeLayout swipeLayout;
        CardView viewForeground;

        private listViewHolder(final View itemView) {
            super(itemView);
            serviceTv =  itemView.findViewById(R.id.serviceTv);
            timeTV =  itemView.findViewById(R.id.timeTv);
            stylistNameTv = itemView.findViewById(R.id.stylistnameTV);
            dateTv =itemView.findViewById(R.id.dateTv);
            stylistImg =  itemView.findViewById(R.id.stylistImg);
            Edit =  itemView.findViewById(R.id.editImg);
            swipeLayout =  itemView.findViewById(R.id.swipe);
            Delete =  itemView.findViewById(R.id.deleteImg);

            lastname=itemView.findViewById(R.id.stylistLastnameTV);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

    }
    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());
        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }
        return outputDate;
    }
}
