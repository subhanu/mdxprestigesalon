package com.example.dis015.mdxprestigesalon5;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import ModelClass.CustomersCollection;


public class CustomerRegistration extends AppCompatActivity {
    TextView agreeServices,agreeTv,terms,genderEdt;
    EditText firstnameEdt,lastnameEdt,dobEdt,mobilenoEdt;
    Button registrationBtn,uploadBtn,chooseBtn;
    String gender[]={"Male","Female","Others"};
    FirebaseAuth mAuth;
    private Uri imageUri;
    ImageView userImage;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;
    private  final int CAMERA_REQUEST_CODE=71;
    String strMobileno,strAge,strGender,strFirstname,strLastname,strUserid,imageUrl;
    Spinner genderSpinner;
    RelativeLayout siginTv;
    ProgressBar progressBar;
    Date dob;
    CustomersCollection customersCollection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        genderSpinner=findViewById(R.id.genderSpinner);
        mobilenoEdt=findViewById(R.id.mobilenoEdt);
        firstnameEdt=findViewById(R.id.firstNameEdt);
        uploadBtn=findViewById(R.id.uploadBtn);
        registrationBtn=findViewById(R.id.registerBtn);
        lastnameEdt=findViewById(R.id.lastNameEdt);
        userImage=findViewById(R.id.userimage);
        dobEdt=findViewById(R.id.dobEdt);
        agreeServices=findViewById(R.id.acessServicesTV);
        agreeTv=findViewById(R.id.agreeTv);
        terms=findViewById(R.id.termsTv);
        genderEdt= findViewById(R.id.genderEdt) ;
        progressBar=findViewById(R.id.Progressbar);

        mAuth= FirebaseAuth.getInstance();
        firebaseFirestore= FirebaseFirestore.getInstance();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        storageReference= firebaseStorage.getReference();

        strMobileno=getIntent().getExtras().getString("mobilenumber");
        mobilenoEdt.setText(strMobileno);

        registerDetails();

    }
    public void registerDetails(){

        agreeServices.setText(R.string.agreeconditons);
        agreeTv.setText(R.string.agreeTerms);

        try {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, gender);
            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            genderSpinner.setAdapter(dataAdapter);
            genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                    genderEdt.setText(item);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        registrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    validate();
                    if((dob!=null && strFirstname!=null && imageUrl!=null && strLastname!=null)){
                        progressBar.setVisibility(View.VISIBLE);
                        strMobileno=mobilenoEdt.getText().toString().trim();
                        strFirstname=firstnameEdt.getText().toString().trim();
                        strGender=genderEdt.getText().toString().trim();
                        strLastname=lastnameEdt.getText().toString().trim();
                        strAge=dobEdt.getText().toString().trim();
                        strUserid=mAuth.getUid();
                        strUserid=mAuth.getUid();
                        customersCollection=new CustomersCollection(new String(),"mobile",dob,strFirstname,imageUrl,strGender,strLastname,strMobileno,strUserid,new ArrayList<String>());
                        firebaseFirestore.collection("customers").add(customersCollection).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                                startActivity(intent);
                                finish();
                                progressBar.setVisibility(View.GONE);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                String error=e.getMessage();
                                (Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG)).show();
                            }
                        });

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Image"),CAMERA_REQUEST_CODE);
            }
        });

        dobEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePicker = new DatePickerDialog(CustomerRegistration.this,R.style.DateTheme,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strdate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                        String strDate=formateDateFromstring("yyyy-MM-dd","yyyy-MM-dd'T'HH:mm:ss Z",strdate);
                        dob=getDateFromString(strDate);
                        dobEdt.setText(strdate);

                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePicker.show();
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CAMERA_REQUEST_CODE & resultCode==RESULT_OK && data != null && data.getData() != null){
            //Progress bar to show
            imageUri=data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                userImage.setImageBitmap(bitmap);
                upload();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void upload(){
        if(imageUri!=null){
            final StorageReference reference=storageReference.child("customers/"+ UUID.randomUUID().toString());
            reference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Log.e("imageurl", "uri: " + uri.toString());
                            imageUrl=uri.toString();
                            //Handle whatever you're going to do with the URL here
                        }
                    });
//                    Toast.makeText(getApplicationContext(),"File Uploaded",Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                    double totalProgress=(100*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();

//                    Toast.makeText(getApplicationContext(),"File uploaded"+(int)totalProgress,Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d("exception", "ParseException - dateFormat");
        }
        return outputDate;

    }
    static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
    public Date getDateFromString(String datetoSaved){
        try {
            Date date = format.parse(datetoSaved);
            return date ;
        } catch (ParseException e){
            return null ;
        }
    }

    public boolean validate() {
        boolean valid = true;

        strMobileno=mobilenoEdt.getText().toString().trim();
        strFirstname=firstnameEdt.getText().toString().trim();
        strGender=genderEdt.getText().toString().trim();
        strLastname=lastnameEdt.getText().toString().trim();
        strAge=dobEdt.getText().toString().trim();

        if (strMobileno.isEmpty()) {
            mobilenoEdt.setError("Indtast dit mobilNummer");
            valid = false;
        } else {
            mobilenoEdt.setError(null);
        }
        if (strFirstname.isEmpty()) {
            firstnameEdt.setError("Indtast dit fornavn");
            valid = false;
        } else {
            firstnameEdt.setError(null);
        }

        if (strLastname.isEmpty()) {
            lastnameEdt.setError("Indtast dit efternavn");
            valid = false;
        } else {
            lastnameEdt.setError(null);
        }

        if (strGender.isEmpty()) {
            genderEdt.setError("Vælg dit køn");
            valid = false;
        } else {
            genderEdt.setError(null);
        }

        if (strAge.isEmpty()) {
            dobEdt.setError("Vælg fødselsdato");
            valid = false;
        } else {
            dobEdt.setError(null);
        }
        if (imageUrl==null) {
            selectProfilePicture();
            valid = false;
        } else {
            Log.d("imageview",imageUrl);
        }
        return valid;
    }

    private void selectProfilePicture(){
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("Upload Profile Picture");
        builder.setMessage("Please select profile picture");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
}
