package Adapter;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import FirstFlowFragments.ServiceCategoryFragment;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DIS015 on 3/8/2018.
 */

public class StylistAdapter extends RecyclerView.Adapter {

    private final Task<QuerySnapshot> task;
    private String documentid;
    private ArrayList<ArrayList<String>> allServices=new ArrayList<>();
    private ArrayList<String> stylistId;
    private CircleImageView stylistImg;


    public StylistAdapter(Task<QuerySnapshot> task, String id, ArrayList<String> stylistId, CircleImageView stylistImg) {
        this.task = task;
        this.documentid=id;
        this.stylistId=stylistId;
        this.stylistImg=stylistImg;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem, parent, false);
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        String imageUrl = task.getResult().getDocuments().get(position).getString("imageUrl");
        String firstName = task.getResult().getDocuments().get(position).getString("firstName");
        String lastName = task.getResult().getDocuments().get(position).getString("lastName");
//        Picasso.get().load(imageUrl).resize(120,110).into(((listViewHolder) holder).stylistImg);
        Picasso.get().load(imageUrl).into(((listViewHolder) holder).stylistImg);
        ((listViewHolder) holder).stylistname.setText(firstName);
        ((listViewHolder) holder).lastnameTv.setText(lastName);
        ArrayList<String> serviceDetails = (ArrayList<String>) task.getResult().getDocuments().get(position).get("services");
        allServices.add(serviceDetails);
        ((listViewHolder) holder).ChooseBtn.setBackgroundResource(R.drawable.mybutton);
    }

    @Override
    public int getItemCount() {
        return task.getResult().getDocuments().size();
    }

    private class listViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView stylistname,lastnameTv,specialist;
        private ImageView stylistImg;
        private Button ChooseBtn;

        @SuppressLint("ResourceAsColor")
        public listViewHolder(View itemView) {
            super(itemView);

            stylistImg = itemView.findViewById(R.id.stylistImg);
            stylistname = itemView.findViewById(R.id.StylistTv);
            lastnameTv =  itemView.findViewById(R.id.lastnameTv);
            specialist = itemView.findViewById(R.id.specialistTv);
            ChooseBtn =  itemView.findViewById(R.id.chooseBtn);
            ChooseBtn.setOnClickListener(this);
            }

        @SuppressLint("ResourceAsColor")
        public void onClick(View view) {
            ChooseBtn.setBackgroundResource(R.drawable.selectedbutton);
            try {
              ServiceCategoryFragment servicesFragment = new ServiceCategoryFragment();
                Bundle stylist = new Bundle();
                stylist.putString("stylistfname", task.getResult().getDocuments().get(getAdapterPosition()).getString("firstName"));
                stylist.putString("stylistlname", task.getResult().getDocuments().get(getAdapterPosition()).getString("lastName"));
                stylist.putString("stylistImg", task.getResult().getDocuments().get(getAdapterPosition()).getString("imageUrl"));
                stylist.putString("stylistid",stylistId.get(getAdapterPosition()));
                stylist.putStringArrayList("serviceid",(ArrayList<String>)task.getResult().getDocuments().get(getAdapterPosition()).get("services"));
                stylist.putString("documentid",documentid);
                servicesFragment.setArguments(stylist);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, servicesFragment).addToBackStack(null).commit();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}






