package Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;

public class DifferentGenderServicesAdapter2 extends RecyclerView.Adapter {

    private Task<QuerySnapshot> task;
    private ArrayList<String> serviceArray = new ArrayList<>();
    private ArrayList<Integer> costArray = new ArrayList<>();
    private ArrayList<ArrayList<String>> stylistIdArray = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> intDurationArr=new ArrayList<>();

   private TextView serviceTv, durationTv;
   private int count = 0;
    private ArrayList<String> serviceDocumentId=new ArrayList<>();
    private ArrayList<String> selectedServiceId=new ArrayList<>();

    private ArrayList<String> selectedServiceArray=new ArrayList<>();

    private ArrayList<Integer> selectedServiceCost=new ArrayList<>();
    private ArrayList<String> stylist=new ArrayList<>();
    private ArrayList<Integer> intDuration=new ArrayList<>();
    private ArrayList<ArrayList<String>> selectedStylistId=new ArrayList<>();
    private ArrayList<ArrayList<Integer>> selectedDuration=new ArrayList<>();
    private TextView error_tv;

    public DifferentGenderServicesAdapter2(Task<QuerySnapshot> task,
                                           ArrayList<String> serviceDocumentId,
                                           TextView servicesTv,
                                           TextView durationTv, TextView error_tv) {
        this.task = task;
        this.serviceTv = servicesTv;
        this.durationTv = durationTv;
        this.serviceDocumentId=serviceDocumentId;
        this.error_tv=error_tv;
        count = task.getResult().size();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_item, parent, false);
        return new DifferentGenderServicesAdapter2.listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        try {
            ((listViewHolder) holder).servicenameTv.setText(task.getResult().getDocuments().get(position).get("name").toString());
            ((listViewHolder) holder).serviceCost.setText(task.getResult().getDocuments().get(position).get("price").toString());

            String servicename = task.getResult().getDocuments().get(position).get("name").toString();
            Integer price = task.getResult().getDocuments().get(position).getLong("price").intValue();
            serviceArray.add(servicename);
            costArray.add(price);

            ArrayList<HashMap<String,String>> stylistArr=new ArrayList<>();
            stylistArr=(ArrayList<HashMap<String, String>>)task.getResult().getDocuments().get(position).get("services");
            final ArrayList<String> stylistsId = new ArrayList<String>();
            ArrayList<Integer> durationArray=new ArrayList<>();


            stylistsId.clear();
            durationArray.clear();

            for (int i=0;i<stylistArr.size();i++){
                String strStylist = stylistArr.get(i).get("stylist").toString();
                String strDuration = stylistArr.get(i).get("duration").toString();
                stylistsId.add(strStylist);
                durationArray.add(Integer.parseInt(strDuration));
            }
            stylistIdArray.add(stylistsId);
            intDurationArr.add(durationArray);

            ((listViewHolder) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(((listViewHolder) holder).checkBox.isChecked()){
                        error_tv.setVisibility(View.GONE);
                            String service=serviceArray.get(position);
                            Integer cost=costArray.get(position);
                            String serviceid=serviceDocumentId.get(position);
                            stylist=stylistIdArray.get(position);
                            intDuration=intDurationArr.get(position);

                            selectedServiceId.add(serviceid);
                            selectedDuration.add(intDuration);
                            selectedStylistId.add(stylist);
                            selectedServiceCost.add(cost);
                            selectedServiceArray.add(service);

                        // restrict the user to select only two services
                        //if more than two disable the selected checkbox
                        if(selectedServiceArray.size()==3){
                            ((listViewHolder) holder).checkBox.setChecked(false);
                            selectedServiceArray.remove(service);
                            selectedStylistId.remove(stylist);
                            selectedDuration.remove(intDuration);
                            selectedServiceId.remove(serviceid);
                            selectedServiceCost.remove(cost);
                        }


                            if(selectedServiceArray.size()==2){
                            error_tv.setVisibility(View.GONE);
                                serviceTv.setText(selectedServiceArray.get(0)+" "+"+"+"\n"+selectedServiceArray.get(1));

                            }else {
                                serviceTv.setText(selectedServiceArray.toString().replaceAll("\\[|\\]", ""));

                            }


                    }else {
                        String service=serviceArray.get(position);
                        Integer cost=costArray.get(position);
                        String serviceid=serviceDocumentId.get(position);
                        stylist=stylistIdArray.get(position);
                        intDuration=intDurationArr.get(position);

                        selectedServiceId.remove(serviceid);
                        selectedStylistId.remove(stylist);
                        selectedDuration.remove(intDuration);
                        selectedServiceCost.remove(cost);
                        selectedServiceArray.remove(service);
                        serviceTv.setText(selectedServiceArray.toString().replaceAll("\\[|\\]", ""));

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getServiceId() {
        return selectedServiceId;
    }

    public ArrayList<String> getServices() {
        return selectedServiceArray;
    }

    public Integer getCostArray() {
        Integer cost = null;
        if(selectedServiceCost.size()>1){
            cost=selectedServiceCost.get(0)+selectedServiceCost.get(1);
        }else {
            cost=selectedServiceCost.get(0);
        }
        return cost;
    }

    public ArrayList<ArrayList<Integer>> getSelectedDuration() {
        return selectedDuration;
    }

    public ArrayList<ArrayList<String>> getSelectedStylistId() {
        return selectedStylistId;
    }

    @Override
    public int getItemCount() {
        return task.getResult().size();
    }
    private class listViewHolder extends RecyclerView.ViewHolder {

        private CheckBox checkBox;
        private TextView servicenameTv, serviceCost;

        public listViewHolder(final View itemView) {
            super(itemView);
            try {
                servicenameTv = itemView.findViewById(R.id.damerServiceTV);
                serviceCost =  itemView.findViewById(R.id.damerRsTV);
                checkBox =  itemView.findViewById(R.id.checkBox);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
