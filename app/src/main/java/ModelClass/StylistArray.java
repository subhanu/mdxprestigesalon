package ModelClass;

import android.os.Parcel;


public class StylistArray {

    private String stylist;
    private String endTime;

    public StylistArray(String stylist, String endTime) {
        this.stylist = stylist;
        this.endTime = endTime;
    }

    protected StylistArray(Parcel in) {
        stylist = in.readString();
        endTime = in.readString();
    }


    public String getStylist() {
        return stylist;
    }

    public void setStylist(String stylist) {
        this.stylist = stylist;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
