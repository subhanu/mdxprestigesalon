package FirstFlowFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.ServiceAdapter1;
import ModelClass.ServiceDetailsCollection;
import ModelClass.Services_childrenCollection;
import ModelClass.Services_menCollection;
import ModelClass.Services_womanCollection;
import ModelClass.StylistDetails2;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceCategoryFragment extends Fragment {

    String stylistFirstname,strdocumentId,stylistLastName,stylistid,strDuration;
    CircleImageView imageView;
    TextView stylistNameTv,servicesTv,durationTv,error_tv;
    Button serviceButton;
    RecyclerView recyclerView;
    private Bundle bundle;
    Serializable servicesFor=new ArrayList<>();
    LinearLayout menLayout,womenLayout,childrenLayout;
    ArrayList<String> servicesid=new ArrayList<>();
    ServiceAdapter1 serviceAdapter1;
    int intServiceDuration;
    ImageView backImg;

    private ArrayList<HashMap<String,String>> menStylistArr=new ArrayList<>();
    private ArrayList<HashMap<String,String>> womenStylistArr=new ArrayList<>();
    private ArrayList<HashMap<String,String>> childrenStylistArr=new ArrayList<>();

    ArrayList<ArrayList<String>> selectedStylidIdArray=new ArrayList<>();
    ArrayList<ArrayList<String>> selectedDurationArray=new ArrayList<>();

    List<Services_menCollection> services_menCollections;
    List<Services_womanCollection> services_womanCollections;
    List<Services_childrenCollection> services_childrenCollections;
    List<ServiceDetailsCollection> serviceDetailsCollectionList;



    public ServiceCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.servicesfrag, container, false);
        try {
            stylistNameTv =  view.findViewById(R.id.stylistNameTv);
            servicesTv =  view.findViewById(R.id.serviceTv);
            durationTv = view.findViewById(R.id.durationTv);
            imageView = view.findViewById(R.id.stylistImg);
            serviceButton =  view.findViewById(R.id.servicesBtn);
            menLayout=view.findViewById(R.id.menLayout);
            womenLayout=view.findViewById(R.id.womenLayout);
            childrenLayout=view.findViewById(R.id.childrenLayout);
            recyclerView =  view.findViewById(R.id.stylistRecycler);
            backImg=view.findViewById(R.id.back_Img);
            error_tv=view.findViewById(R.id.error_tv);


            //initialize the list
            services_menCollections=new ArrayList<>();
            services_womanCollections=new ArrayList<>();
            services_childrenCollections=new ArrayList<>();
            serviceDetailsCollectionList=new ArrayList<>();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try{
            bundle = getArguments();
            if (bundle != null) {
                stylistFirstname = bundle.getString("stylistfname", "");
                stylistLastName=bundle.getString("stylistlname","");
                servicesFor = getArguments().getStringArrayList("specialist");
                stylistid=getArguments().getString("stylistid","");
                strdocumentId=bundle.getString("documentid","");
                servicesid = getArguments().getStringArrayList("serviceid");

            }
            stylistNameTv.setText(stylistFirstname);
            Picasso.get().load(bundle.getString("stylistImg")).into(imageView);
        }catch (Exception e){
            e.printStackTrace();
        }


        FirebaseFirestore firebaseFirestore= FirebaseFirestore.getInstance();
        firebaseFirestore.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (DocumentSnapshot documentSnapshot : task.getResult()) {
                        ArrayList<String> stylistIdArray = new ArrayList<>();
                        ArrayList<String> durationArr = new ArrayList<>();

                        String documentid = documentSnapshot.getId();
                        String serviceNames = documentSnapshot.getString("name");
                        Integer intPrice = documentSnapshot.getLong("price").intValue();
                        menStylistArr = (ArrayList<HashMap<String, String>>) documentSnapshot.get("services");

                        for (int i = 0; i < menStylistArr.size(); i++) {
                            String stylist = menStylistArr.get(i).get("stylist");
                            String strDuration = menStylistArr.get(i).get("duration");
                            stylistIdArray.add(stylist);
                            durationArr.add(strDuration);
                        }

                        Services_menCollection services_menCollection=new Services_menCollection(documentid,serviceNames,intPrice,durationArr,stylistIdArray);
                        services_menCollections.add(services_menCollection);
                        }

                    menLayout.setBackgroundResource(R.drawable.white_background);
                    womenLayout.setBackgroundResource(R.color.darkgrey);
                    childrenLayout.setBackgroundResource(R.color.darkgrey);


                    ArrayList<String> stylistIDArray=new ArrayList<>();
                    ArrayList<String> durationArray=new ArrayList<>();

                    for(int i=0;i<servicesid.size();i++){
                        for(int j=0;j<services_menCollections.size();j++){
                            if(servicesid.get(i).contains(services_menCollections.get(j).getServiceId())){
                                String strServiceName=services_menCollections.get(j).getName();
                                Integer strServiceCost=services_menCollections.get(j).getPrice();
                                String serviceId=services_menCollections.get(j).getServiceId();
                                stylistIDArray=services_menCollections.get(j).getStylist();
                                durationArray=services_menCollections.get(j).getDuration();

                                ServiceDetailsCollection services_menCollection=new ServiceDetailsCollection(serviceId,strServiceName,strServiceCost,durationArray,stylistIDArray);
                                serviceDetailsCollectionList.add(services_menCollection);

                            }
                        }
                    }
                    serviceAdapter1 = new ServiceAdapter1(servicesTv,serviceDetailsCollectionList,error_tv );
                    recyclerView.setAdapter(serviceAdapter1);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);

                }
            }
        });

        firebaseFirestore.collection("services_women").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot documentSnapshot : task.getResult()) {
                        ArrayList<String> stylistIdArray = new ArrayList<>();
                        ArrayList<String> durationArr = new ArrayList<>();

                        String documentid = documentSnapshot.getId();
                        String serviceNames = documentSnapshot.getString("name");
                        Integer intPrice = documentSnapshot.getLong("price").intValue();
                        womenStylistArr = (ArrayList<HashMap<String, String>>) documentSnapshot.get("services");

                        for (int i = 0; i < womenStylistArr.size(); i++) {
                            String stylist = womenStylistArr.get(i).get("stylist");
                            String strDuration = womenStylistArr.get(i).get("duration");
                            stylistIdArray.add(stylist);
                            durationArr.add(strDuration);
                        }
                        Services_womanCollection services_womanCollection=new Services_womanCollection(documentid,serviceNames,intPrice,durationArr,stylistIdArray);
                        services_womanCollections.add(services_womanCollection);
                        }
                }
            }
        });

        firebaseFirestore.collection("services_children").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (DocumentSnapshot documentSnapshot : task.getResult()) {
                        ArrayList<String> stylistIdArray = new ArrayList<>();
                        ArrayList<String> durationArr = new ArrayList<>();

                        String documentid = documentSnapshot.getId();
                        String serviceNames = documentSnapshot.getString("name");
                        Integer intPrice = documentSnapshot.getLong("price").intValue();
                        childrenStylistArr = (ArrayList<HashMap<String, String>>) documentSnapshot.get("services");

                        for (int i = 0; i < childrenStylistArr.size(); i++) {
                            String stylist = childrenStylistArr.get(i).get("stylist");
                            String strDuration = childrenStylistArr.get(i).get("duration");
                            stylistIdArray.add(stylist);
                            durationArr.add(strDuration);
                        }
                        Services_childrenCollection services_childrenCollection=new Services_childrenCollection(documentid,serviceNames,intPrice,durationArr,stylistIdArray);
                        services_childrenCollection.setServiceId(documentid);
                        services_childrenCollections.add(services_childrenCollection);
                        }
                }
            }
        });

        menLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                serviceDetailsCollectionList.clear();
                menLayout.setBackgroundResource(R.drawable.white_background);
                womenLayout.setBackgroundResource(R.color.darkgrey);
                childrenLayout.setBackgroundResource(R.color.darkgrey);

                ArrayList<String> stylistIdArray=new ArrayList<>();
                ArrayList<String> durationArray=new ArrayList<>();

                for(int i=0;i<servicesid.size();i++){
                    for(int j=0;j<services_menCollections.size();j++){
                        if(servicesid.get(i).contains(services_menCollections.get(j).getServiceId())){
                            String strServiceName=services_menCollections.get(j).getName();
                            Integer strServiceCost=services_menCollections.get(j).getPrice();
                            String serviceId=services_menCollections.get(j).getServiceId();
                            stylistIdArray=services_menCollections.get(j).getStylist();
                            durationArray=services_menCollections.get(j).getDuration();

                            ServiceDetailsCollection services_menCollection=new ServiceDetailsCollection(serviceId,strServiceName,strServiceCost,durationArray,stylistIdArray);
                            serviceDetailsCollectionList.add(services_menCollection);

                        }
                    }
                }

                serviceAdapter1 = new ServiceAdapter1(servicesTv,serviceDetailsCollectionList, error_tv);
                recyclerView.setAdapter(serviceAdapter1);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(layoutManager);

            }
        });

        womenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceDetailsCollectionList.clear();
                womenLayout.setBackgroundResource(R.drawable.white_background);
                menLayout.setBackgroundResource(R.color.darkgrey);
                childrenLayout.setBackgroundResource(R.color.darkgrey);

                ArrayList<String> stylistIdArray=new ArrayList<>();
                ArrayList<String> durationArray=new ArrayList<>();

                for(int i=0;i<servicesid.size();i++){
                    for(int j=0;j<services_womanCollections.size();j++){
                        if(servicesid.get(i).contains(services_womanCollections.get(j).getServiceId())){
                            String strServiceName=services_womanCollections.get(j).getName();
                            Integer strServiceCost=services_womanCollections.get(j).getPrice();
                            String serviceId=services_womanCollections.get(j).getServiceId();
                            stylistIdArray=services_womanCollections.get(j).getStylist();
                            durationArray=services_womanCollections.get(j).getDuration();

                            ServiceDetailsCollection serviceDetailsCollection=new  ServiceDetailsCollection(serviceId,strServiceName,strServiceCost,durationArray,stylistIdArray);
                            serviceDetailsCollectionList.add(serviceDetailsCollection);

                        }
                    }
                }

                serviceAdapter1 = new ServiceAdapter1(servicesTv,serviceDetailsCollectionList, error_tv);
                recyclerView.setAdapter(serviceAdapter1);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(layoutManager);

            }
        });

        childrenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 serviceDetailsCollectionList.clear();
                childrenLayout.setBackgroundResource(R.drawable.white_background);
                womenLayout.setBackgroundResource(R.color.darkgrey);
                menLayout.setBackgroundResource(R.color.darkgrey);

                ArrayList<String> stylistIdArray=new ArrayList<>();
                ArrayList<String> durationArray=new ArrayList<>();

                for(int i=0;i<servicesid.size();i++){
                    for(int j=0;j<services_childrenCollections.size();j++){
                        if(servicesid.get(i).contains(services_childrenCollections.get(j).getServiceId())){
                            String strServiceName=services_childrenCollections.get(j).getName();
                            Integer strServiceCost=services_childrenCollections.get(j).getPrice();
                            String serviceId=services_childrenCollections.get(j).getServiceId();
                            stylistIdArray=services_childrenCollections.get(j).getStylist();
                            durationArray=services_childrenCollections.get(j).getDuration();

                            ServiceDetailsCollection serviceDetailsCollection=new  ServiceDetailsCollection(serviceId,strServiceName,strServiceCost,durationArray,stylistIdArray);
                            serviceDetailsCollectionList.add(serviceDetailsCollection);
                            }
                    }
                }

                serviceAdapter1 = new ServiceAdapter1(servicesTv,serviceDetailsCollectionList, error_tv);
                recyclerView.setAdapter(serviceAdapter1);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(layoutManager);
                }
        });

        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedStylidIdArray=serviceAdapter1.getSelectedStylistIdArray();
                selectedDurationArray=serviceAdapter1.getSelectedDurArray();


                ArrayList<String> stylistId = new ArrayList<>();
                ArrayList<String> duration = new ArrayList<>();

                for(ArrayList<String> l : selectedStylidIdArray) {
                    for(String s : l) {
                        stylistId.add(s);
                    }
                }

                for(ArrayList<String> l : selectedDurationArray ) {
                    for(String s : l) {
                        duration.add(s);
                    }
                }
                endDuration(stylistid,stylistId,duration);

                StylistDetails2 stylistDetails = new StylistDetails2(stylistid,serviceAdapter1.getServices(),serviceAdapter1.getSelectedServiceIdArray(),intServiceDuration,serviceAdapter1.getIntServicePrice(),bundle.getString("stylistImg"),bundle.getString("stylistfname"),strdocumentId);
                BookAppointmentFragment bookAppointmentFragment = new BookAppointmentFragment();
                Bundle bundle = getArguments();
                bundle.putParcelable("stylistflow", stylistDetails);
                bookAppointmentFragment.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.frameLayout, bookAppointmentFragment).addToBackStack(null).commit();
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StylistFragment stylistFragment = new StylistFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, stylistFragment).addToBackStack(null).commit();

            }
        });



        return view;
    }

    public void endDuration(String searchedKey, ArrayList<String> keys, ArrayList<String> values) {
        int i;
        ArrayList<String> endDuration=new ArrayList<>();
        for (i = 0; i < keys.size(); i++) {
            if ((keys.get(i)).equals(searchedKey)){
                strDuration= values.get(i);
                endDuration.add(strDuration);
                if(endDuration.size()>=2){
                    intServiceDuration=( Integer.parseInt(endDuration.get(0))+ Integer.parseInt(endDuration.get(1)));
                }else{
                    intServiceDuration= Integer.parseInt(endDuration.get(0));
                }
            }

        }
    }


}
