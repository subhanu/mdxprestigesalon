package Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.example.dis015.mdxprestigesalon5.R;
import java.util.ArrayList;
import java.util.List;
import ModelClass.ServiceDetailsCollection;


public class ServiceAdapter1  extends RecyclerView.Adapter {

    private TextView servicesTv;
    private List<ServiceDetailsCollection> servicesDetailsCollections;
    private ArrayList<String> services=new ArrayList<>();
    private ArrayList<ArrayList<String>> selectedStylistIdArray=new ArrayList<>();
    private ArrayList<ArrayList<String>> selectedDurArray=new ArrayList<>();
    private ArrayList<String> selectedServiceIdArray=new ArrayList<>();
    private ArrayList<Integer> intPriceArray=new ArrayList<>();
   private TextView error_tv;

    public ServiceAdapter1(TextView servicesTv,
                           List<ServiceDetailsCollection> services_menCollections,
                           TextView error_tv) {
        this.servicesTv=servicesTv;
        this.servicesDetailsCollections=services_menCollections;
        this.error_tv=error_tv;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_item, parent, false);
        return new listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        ((listViewHolder)holder).servicenameTv.setText(servicesDetailsCollections.get(position).getName());
        ((listViewHolder)holder).serviceCost.setText(Integer.toString(servicesDetailsCollections.get(position).getPrice()));

        ((listViewHolder)holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                ArrayList<String> selectedStylistId=new ArrayList<>();
                ArrayList<String> selectedDuration=new ArrayList<>();

                if(((listViewHolder) holder).checkBox.isChecked()){
                    error_tv.setVisibility(View.GONE);
                    String service=servicesDetailsCollections.get(position).getName();
                    Integer intPrice=servicesDetailsCollections.get(position).getPrice();
                    String serviceid=servicesDetailsCollections.get(position).getId();
                    selectedStylistId=servicesDetailsCollections.get(position).getStylistArray();
                    selectedDuration=servicesDetailsCollections.get(position).getDurationArray();

                    services.add(service);
                    selectedStylistIdArray.add(selectedStylistId);
                    selectedDurArray.add(selectedDuration);
                    selectedServiceIdArray.add(serviceid);
                    intPriceArray.add(intPrice);

                    // restrict the user to select only two services
                    //if more than two disable the selected checkbox
                    if(services.size()==3){
                        ((listViewHolder) holder).checkBox.setChecked(false);
                        services.remove(service);
                        selectedStylistIdArray.remove(selectedStylistId);
                        selectedDurArray.remove(selectedDuration);
                        selectedServiceIdArray.remove(serviceid);
                        intPriceArray.remove(intPrice);
                    }
                    if(services.size()==2){
                        error_tv.setVisibility(View.GONE);
                        servicesTv.setText(services.get(0)+" "+"+"+"\n"+services.get(1));
                    }else {
                        servicesTv.setText(services.get(0));

                    }
                }else {
                    // remove all details when checbox is deselected
                    String service=servicesDetailsCollections.get(position).getName();
                    Integer intPrice=servicesDetailsCollections.get(position).getPrice();
                    String serviceid=servicesDetailsCollections.get(position).getId();
                    selectedStylistId=servicesDetailsCollections.get(position).getStylistArray();
                    selectedDuration=servicesDetailsCollections.get(position).getDurationArray();

                    services.remove(service);
                    selectedStylistIdArray.remove(selectedStylistId);
                    selectedDurArray.remove(selectedDuration);
                    selectedServiceIdArray.remove(serviceid);
                    intPriceArray.remove(intPrice);

                    servicesTv.setText(services.toString().replaceAll("\\[|\\]", ""));

                }

            }
        });

    }

    public int getIntServicePrice() {
        // add price if it is more than 1
        if(intPriceArray.size()==2){
            return intPriceArray.get(0)+intPriceArray.get(1);
        }
        return intPriceArray.get(0);
    }

    public ArrayList<String> getSelectedServiceIdArray() {
        return selectedServiceIdArray;
    }

    public ArrayList<ArrayList<String>> getSelectedDurArray() {
        return selectedDurArray;
    }

    public ArrayList<ArrayList<String>> getSelectedStylistIdArray() {
        return selectedStylistIdArray;
    }

    public ArrayList<String> getServices() {
        return services;
    }


    @Override
    public int getItemCount() {
        return servicesDetailsCollections.size();
    }

    private class listViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkBox;
        private TextView servicenameTv, serviceCost;


        listViewHolder(View itemView) {
            super(itemView);
            try {
                servicenameTv =  itemView.findViewById(R.id.damerServiceTV);
                serviceCost = itemView.findViewById(R.id.damerRsTV);
                checkBox =  itemView.findViewById(R.id.checkBox);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
