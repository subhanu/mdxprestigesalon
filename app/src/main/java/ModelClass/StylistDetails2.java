package ModelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class StylistDetails2 implements Parcelable {
    private String stylistid;
    private ArrayList<String> services;
    private ArrayList<String> servicesId;
    private Integer duration;
    private Integer price;
    private String imageUrl;
    private String firstname;
    private String documentId;

    public StylistDetails2(String stylistid, ArrayList<String> services, ArrayList<String> servicesId, Integer duration, Integer price, String imageUrl, String firstname, String documentId) {
        this.stylistid = stylistid;
        this.services = services;
        this.servicesId = servicesId;
        this.duration = duration;
        this.price = price;
        this.imageUrl = imageUrl;
        this.firstname = firstname;
        this.documentId = documentId;
    }


    protected StylistDetails2(Parcel in) {
        stylistid = in.readString();
        services = in.createStringArrayList();
        servicesId = in.createStringArrayList();
        if (in.readByte() == 0) {
            duration = null;
        } else {
            duration = in.readInt();
        }
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
        imageUrl = in.readString();
        firstname = in.readString();
        documentId = in.readString();
    }

    public static final Creator<StylistDetails2> CREATOR = new Creator<StylistDetails2>() {
        @Override
        public StylistDetails2 createFromParcel(Parcel in) {
            return new StylistDetails2(in);
        }

        @Override
        public StylistDetails2[] newArray(int size) {
            return new StylistDetails2[size];
        }
    };

    public String getStylistid() {
        return stylistid;
    }

    public void setStylistid(String stylistid) {
        this.stylistid = stylistid;
    }

    public ArrayList<String> getServices() {
        return services;
    }

    public void setServices(ArrayList<String> services) {
        this.services = services;
    }

    public ArrayList<String> getServicesId() {
        return servicesId;
    }

    public void setServicesId(ArrayList<String> servicesId) {
        this.servicesId = servicesId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(stylistid);
        parcel.writeStringList(services);
        parcel.writeStringList(servicesId);
        if (duration == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(duration);
        }
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(price);
        }
        parcel.writeString(imageUrl);
        parcel.writeString(firstname);
        parcel.writeString(documentId);
    }
}



