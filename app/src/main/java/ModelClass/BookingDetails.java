package ModelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BookingDetails implements Parcelable {
    private String date;
    private String startTime;
    private ArrayList<String> services;
    private ArrayList<String> servicesId;
    private ArrayList<String> stylistId;
    private ArrayList<String> endTime;
    private ArrayList<Integer> duration;
    private Integer price;

    public BookingDetails(String date, String startTime, ArrayList<String> services, ArrayList<String> servicesId, ArrayList<String> stylistId, ArrayList<String> endTime, ArrayList<Integer> duration, Integer price) {
        this.date = date;
        this.startTime = startTime;
        this.services = services;
        this.servicesId = servicesId;
        this.stylistId = stylistId;
        this.endTime = endTime;
        this.duration = duration;
        this.price = price;
    }

    protected BookingDetails(Parcel in) {
        date = in.readString();
        startTime = in.readString();
        services = in.createStringArrayList();
        servicesId = in.createStringArrayList();
        stylistId = in.createStringArrayList();
        endTime = in.createStringArrayList();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(startTime);
        dest.writeStringList(services);
        dest.writeStringList(servicesId);
        dest.writeStringList(stylistId);
        dest.writeStringList(endTime);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(price);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingDetails> CREATOR = new Creator<BookingDetails>() {
        @Override
        public BookingDetails createFromParcel(Parcel in) {
            return new BookingDetails(in);
        }

        @Override
        public BookingDetails[] newArray(int size) {
            return new BookingDetails[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public ArrayList<String> getServices() {
        return services;
    }

    public void setServices(ArrayList<String> services) {
        this.services = services;
    }

    public ArrayList<String> getServicesId() {
        return servicesId;
    }

    public void setServicesId(ArrayList<String> servicesId) {
        this.servicesId = servicesId;
    }

    public ArrayList<String> getStylistId() {
        return stylistId;
    }

    public void setStylistId(ArrayList<String> stylistId) {
        this.stylistId = stylistId;
    }

    public ArrayList<String> getEndTime() {
        return endTime;
    }

    public void setEndTime(ArrayList<String> endTime) {
        this.endTime = endTime;
    }

    public ArrayList<Integer> getDuration() {
        return duration;
    }

    public void setDuration(ArrayList<Integer> duration) {
        this.duration = duration;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}

