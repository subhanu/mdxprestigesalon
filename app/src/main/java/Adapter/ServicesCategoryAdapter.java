package Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QuerySnapshot;

public class ServicesCategoryAdapter extends RecyclerView.Adapter {

   private Task<QuerySnapshot> task;

    public ServicesCategoryAdapter(Task<QuerySnapshot> task) {
        this.task=task;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.services_category,parent,false);
        return new ServicesCategoryAdapter.listViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((listViewHolder)holder).servicename.setText(task.getResult().getDocuments().get(position).getString("name"));
        ((listViewHolder)holder).serviceCost.setText(String.valueOf(task.getResult().getDocuments().get(position).getLong("price").intValue()));

    }

    @Override
    public int getItemCount() {
        return task.getResult().size();
    }

    private class listViewHolder extends RecyclerView.ViewHolder{
        private TextView servicename,serviceCost;

        public listViewHolder(View itemView){
            super(itemView);
            servicename = itemView.findViewById(R.id.damerServiceTV);
            serviceCost=itemView.findViewById(R.id.damerRsTV);

        }
    }

}
