package ModelClass;

import android.os.Parcel;
import android.os.Parcelable;

public class StylistCollection implements Parcelable {
    private String stylistid;
    private String firstName;
    private String lastName;
    private String imageUrl;

    public StylistCollection(String stylistid, String firstName, String lastName, String imageUrl) {
        this.stylistid = stylistid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
    }

    protected StylistCollection(Parcel in) {
        stylistid = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<StylistCollection> CREATOR = new Creator<StylistCollection>() {
        @Override
        public StylistCollection createFromParcel(Parcel in) {
            return new StylistCollection(in);
        }

        @Override
        public StylistCollection[] newArray(int size) {
            return new StylistCollection[size];
        }
    };

    public String getStylistid() {
        return stylistid;
    }

    public void setStylistid(String stylistid) {
        this.stylistid = stylistid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stylistid);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imageUrl);
    }
}
