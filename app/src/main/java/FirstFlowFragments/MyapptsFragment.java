package FirstFlowFragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dis015.mdxprestigesalon5.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.BookingsAdapter;
import ModelClass.BookingsCollection;
import ModelClass.ServicesCollection;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyapptsFragment extends Fragment {

    TextView serviceTv;
    ImageView StylistImg;
    private RecyclerView recyclerView;
    String userid;
    FirebaseAuth mAuth;
    //    BookAppointmentAdapter bookAppointmentAdapter;
    String currentUserId;
    RelativeLayout myappointments;
    CoordinatorLayout myapptsLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<String> appointmentList=new ArrayList<>();
    List<BookingsCollection> bookingsCollectionList;
    List<ServicesCollection>servicesCollectionList;
    ImageView backImg;

    public MyapptsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_myaapts, container, false);
        serviceTv=view.findViewById(R.id.serviceTv);
        recyclerView=view.findViewById(R.id.myapptsRecycler);
        StylistImg=view.findViewById(R.id.stylistImg);
        myapptsLayout=view.findViewById(R.id.myappts);
        myappointments=view.findViewById(R.id.Relative);
        swipeRefreshLayout=view.findViewById(R.id.swipe_container);
        backImg=view.findViewById(R.id.back_Img);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        mAuth= FirebaseAuth.getInstance();

        userid=mAuth.getUid();
        bookingsCollectionList=new ArrayList<>();
        servicesCollectionList=new ArrayList<>();

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment homeFragment = new HomeFragment();
                getFragmentManager().beginTransaction().replace(R.id.frameLayout, homeFragment).addToBackStack(null).commit();

            }
        });



        db.collection("services_men").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot documentSnapshot:task.getResult()){
                        String id=documentSnapshot.getId();
                        String strServiceName=documentSnapshot.getString("name");

                        ServicesCollection servicesCollection=new ServicesCollection(strServiceName,id);
                        servicesCollectionList.add(servicesCollection);

                    }

                }
            }
        });

        db.collection("services_women").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot documentSnapshot:task.getResult()){
                        String id=documentSnapshot.getId();
                        String strServiceName=documentSnapshot.getString("name");

                        ServicesCollection servicesCollection=new ServicesCollection(strServiceName,id);
                        servicesCollectionList.add(servicesCollection);
                    }

                }
            }
        });

        db.collection("services_children").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot documentSnapshot:task.getResult()){
                        String id=documentSnapshot.getId();
                        String strServiceName=documentSnapshot.getString("name");

                        ServicesCollection servicesCollection=new ServicesCollection(strServiceName,id);
                        servicesCollectionList.add(servicesCollection);
                    }

                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshBookings();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        loadData();
        return view;
    }

    public void refreshBookings(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("bookings").whereEqualTo("customer",userid).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for (DocumentSnapshot document : task.getResult()) {
                    Log.d("ID", document.getId() + " => " + document.getData());
                    currentUserId=document.getId();
                    appointmentList.add(currentUserId);
                }
            }
        });

    }

    private void loadData(){
//        setupAdapter();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("bookings").whereEqualTo("customer",mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for (DocumentSnapshot document : task.getResult()) {
                    Log.d("ID", document.getId() + " => " + document.getData());
                    String bookingId=document.getId();
                    String bookingBy=document.getString("bookingBy");
                    String bookingType=document.getString("bookingType");
                    String comments=document.getString("comments");
                    String customer=document.getString("customer");
                    String date=document.getString("date");
                    Integer duration=document.getLong("duration").intValue();
                    String endTime=document.getString("endTime");
                    String imageUrl=document.getString("imageUrl");
                    Integer price=document.getLong("price").intValue();
                    HashMap<String,String> recurring = (HashMap<String,String>) document.get("recurring");
                    String recurringId=document.getString("recurringId");
                    ArrayList<String> serviceDetails = (ArrayList<String>) document.get("services");
                    String startTime=document.getString("startTime");
                    String stylistId=document.getString("stylist");
                    BookingsCollection bookingsCollection=new BookingsCollection(bookingBy,bookingType,comments,
                            customer,date,duration,endTime,imageUrl,price,recurring,recurringId,serviceDetails,
                            startTime,stylistId);
                    bookingsCollectionList.add(bookingsCollection);
                    appointmentList.add(bookingId);

                    BookingsAdapter bookingsAdapter=new BookingsAdapter(getContext(),bookingsCollectionList,
                            servicesCollectionList,appointmentList);
                    recyclerView.setAdapter(bookingsAdapter);
                    RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                }
            }
        });


    }

    private void setupAdapter(){

    }


}
